/*
 * hk_dbconnection.cpp
 *
 *  Created on: Jun 17, 2014
 *      Author: dinesh
 * Description:
 * 				Implementation of hk_dbconnection.h
 */

#include "../include/hk_dbconnection.h"
#include "../include/utils/utilities.h"

hk_dbconnection::hk_dbconnection()
{
	m_host = "localhost"; m_dbname = "postgres";
	m_user = "postgres"; m_password = "postgres";
	m_port = 5432; m_conn = 0;
	m_connection_live = false;
	m_version = DEFAULT_ELEPHANT_VERSION;
	m_eleid = 1; m_watchTime = 5; m_napTime = 1;
    m_prettySql = false;
}

void hk_dbconnection::SetHost(string host) {
	m_host = host;
}

void hk_dbconnection::SetUser(string user) {
	m_user = user;
}

void hk_dbconnection::SetPort(size_t port) {
	m_port = port;
}

void hk_dbconnection::SetDbname(string dbname) {
	m_dbname = dbname;
}

void hk_dbconnection::SetPassword(string password) {

	m_password = password;
}

void hk_dbconnection::m_SetTruncAfter(bool truncafter) {

	m_truncafter = truncafter;
}

bool hk_dbconnection::m_GetTruncAfter() {

	return m_truncafter;
}

bool hk_dbconnection::m_isPrettySql() {

    return m_prettySql;
}

void hk_dbconnection::m_SetElephantId(string eleid) {

	m_eleid = eleid;
}

void hk_dbconnection::m_SetNapTime(size_t nt) {

	m_napTime = nt;
}

void hk_dbconnection::m_SetWatchTime(size_t wt) {

	m_watchTime = wt;
}

void hk_dbconnection::m_SetFromDate(string fromdate) {

	m_fromdate = fromdate;
}

void hk_dbconnection::m_SetToDate(string todate) {

	m_todate = todate;
}

void hk_dbconnection::m_SetPrettySql(bool ispret) {

    m_prettySql = ispret;
}

void hk_dbconnection::m_SetRepeatRep(string repeatrep) {

    m_repeatRep = repeatrep;
}

void hk_dbconnection::m_SetFileName(string filename) {

    m_filename = filename;
}

string hk_dbconnection::GetHost() {

	return m_host;
}

string hk_dbconnection::GetDbname() {

	return m_dbname;
}

string hk_dbconnection::GetUser() {

	return m_user;
}

string hk_dbconnection::GetPassword() {

	return m_password;
}

size_t hk_dbconnection::GetPort() {

	return m_port;
}
size_t hk_dbconnection::m_GetNapTime() {

	return m_napTime;
}

size_t hk_dbconnection::m_GetWatchTime() {

	return m_watchTime;
}

string hk_dbconnection::m_GetElephantId() {

	return m_eleid;
}

string hk_dbconnection::m_GetFromDate() {

	return m_fromdate;
}

string hk_dbconnection::m_GetToDate() {

	return m_todate;
}

string hk_dbconnection::m_GetRepeatRep() {

    return m_repeatRep;
}

string hk_dbconnection::m_GetRepLocation() {

    return m_replocation+m_GetFileName();
}

void hk_dbconnection::m_SetRepLocation(string repLoc) {

    m_replocation = repLoc;
}

const char* hk_dbconnection::GetConnectionString() {

	strstream cstrng;
	cstrng  << "hostaddr=" << GetHost() << " "
			<< "port=" << GetPort() << " "
			<< "user=" << GetUser() << " "
			<< "dbname=" << GetDbname() << " "
			// Setting the NULL, at the end of the connection string.
			//
			<< "password=" << GetPassword() <<'\0';

	return cstrng.str();
}

string hk_dbconnection::TryToGetConnection() {

	if (!m_connection_live) {
		const char* conn_string = GetConnectionString();
		m_conn = PQconnectdb(conn_string);
		delete[] conn_string;
	}

	if (PQstatus(m_conn)==CONNECTION_OK) {
		m_connection_live = true;
		return "";
	}

	else {
		m_connection_live = false;
		return string(PQerrorMessage(m_conn));
	}
}

void hk_dbconnection::CloseConnection() {
	m_connection_live = false;
	PQfinish(m_conn);
}

pg_conn* hk_dbconnection::m_GetConnection() {

	return m_conn;
}

bool hk_dboperations::m_execCommand(PGconn* conn, string instruct) {

	PGresult* res = PQexec(conn, instruct.c_str());
	if (PQresultStatus(res) == PGRES_COMMAND_OK) {

			PQclear(res);
			return true;
		}
		else {

			PQclear(res);
			return false;
		}
}

vector<string> hk_dboperations::m_execGet1DArray(PGconn* conn, string instruct) {

	vector<string> _1dArray;
	PGresult *res = PQexec(conn, instruct.c_str());

	for (int i=0;i<PQntuples(res); i++)
		_1dArray.push_back(PQgetvalue(res, i, 0));
	PQclear(res);
	return _1dArray;
}

bool hk_dboperations::m_execInsrtUpdte(PGconn* conn, string instruct, string relname) {
	PGresult* res = PQexec(conn,
							("INSERT INTO pghawk." + relname + " " +instruct).c_str());

	if (PQresultStatus(res) == PGRES_COMMAND_OK) {

		PQclear(res);
		return true;
	}
	else {

		PQclear(res);
		return false;
	}
}

string hk_dboperations::m_execGetScalar(PGconn* conn, string instruct) {

	string result;
	PGresult* res = PQexec(conn, ("SELECT " + instruct).c_str());
	result = PQgetvalue(res, 0, 0);
	PQclear(res);
	return result;
}

string hk_dbconnection::m_GetFileName() {

    string filename=m_filename;
    time_t now = time(0);
    tm *date = localtime(&now);

    filename.replace(filename.find("%Y"),2,ToString(1900+date->tm_year));
    filename.replace(filename.find("%M"),2,ToString(date->tm_mon+1));
    filename.replace(filename.find("%D"),2,ToString(date->tm_mday));
    return filename;
}

void hk_dbconnection::m_AdjustFromToDate() {

    if (m_repeatRep!="off") {
     
        string sql= "SELECT UNNEST(ARRAY[TO_CHAR(now(), 'DD-MON-YYYY HH24:MI:SS'), ";
        vector<string> fromToDates;
        
        if (m_repeatRep=="daily") {

            fromToDates = m_execGet1DArray(this->m_GetConnection(), sql + "TO_CHAR(now()-'1 day'::interval, 'DD-MON-YYYY HH24:MI:SS')])");
        }
        
        else if(m_repeatRep=="weekly") {

            fromToDates = m_execGet1DArray(this->m_GetConnection(), sql + "TO_CHAR(now()-'1 week'::interval, 'DD-MON-YYYY HH24:MI:SS')])");
        }
        
        else if(m_repeatRep=="monthly") {

            fromToDates = m_execGet1DArray(this->m_GetConnection(), sql + "TO_CHAR(now()-'1 month'::interval, 'DD-MON-YYYY HH24:MI:SS')])");
        }
        
        m_SetFromDate(fromToDates.at(1));
        m_SetToDate(fromToDates.at(0));
    }
}

hk_dbconnection::~hk_dbconnection() {

	if (m_connection_live)
	{
		PQfinish(m_conn);
		m_conn = NULL;
	}

}
