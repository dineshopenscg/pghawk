/*
 * utilities.cpp
 *
 *   Created on: May 27, 2014
 *       Author: dinesh
 *  Description:
 *  			 Hawk utilities implementations.
 */

#include "../../include/utils/utilities.h"

logger::logger(const char* f) {

	m_Logfile.open(f, ios::app);
	pthread_mutex_init(&m_mutex_lock, NULL);
}

bool logger::m_OpenLogfile(const char* f) {

	// If the logger associated with another file,
	// throw an error, and do return.
	//
	if (m_Logfile.is_open()) {
		cerr<<"Logger is already opened one file."<<endl
			<<"Close the opened one."<<endl;
		return false;
	}
	else {
		m_Logfile.open(f, ios::app);
		return true;
	}
}

void logger::m_LogMessage(Log_Level l, const char* msg, bool appendNewLine) {

	// Log file writer, critical section START.
	//
	pthread_mutex_lock(&m_mutex_lock);
	m_Logfile << GetCurrentTimeStamp();
	switch (l) {

		case ERROR:
				m_Logfile<< "ERROR: "; break;
		case WARNING:
				m_Logfile<< "WARNING: "; break;
		case NOTICE:
				m_Logfile<< "NOTICE: "; break;
		default:
				m_Logfile<< "OTHER: "; break;
	}
	if (appendNewLine)
		m_Logfile << msg << endl;
	else
		m_Logfile << msg;
	// Log file writer, critical section END.
	//
	pthread_mutex_unlock(&m_mutex_lock);
}

bool logger::m_CloseLogfile() {

	m_Logfile.close();
	return true;
}

logger::~logger() {
	m_CloseLogfile();
	pthread_mutex_destroy(&m_mutex_lock);
}


vector<hk_dbconnection> mapParser::m_ParseMapFile(const char* mapFile) {

	FILE* mapFilePointer = fopen(mapFile, "r");
	char *mapLine = NULL, *tmpStr=NULL;
	size_t len=0, eleSetBegin=0, eleSetEnd=0;
	vector<hk_dbconnection> elephants;
	hk_dbconnection elephant;
    
    if (!mapFilePointer) {
        cout << "ERROR: "<<"opening hawk_map.conf file\n";
        cout << "HITN: " <<"make sure you have the above file in /tmp/\n";
        exit(1);
    }

	while (1) {

		if (tmpStr!=NULL || mapLine!=NULL) {
					free(mapLine);
					delete[] tmpStr;
					mapLine = NULL;
					tmpStr = NULL;

				}

		if (getline(&mapLine,&len,mapFilePointer)==-1) {
			free(mapLine);
			mapLine = NULL;
			break;
		}

		if (eleSetBegin && eleSetEnd) {
			//cout << "Ready to prepare another set." << endl;
			eleSetBegin = eleSetEnd = 0;
			elephants.push_back(elephant);
		}

		tmpStr = TrimWhiteSpaces(mapLine);
		// If line is started with #, then don't parse it.
		//
		if (tmpStr[0]=='#' || tmpStr[0]=='\n')
			continue;
		// If we found the '[' character, then elephant set has began
		//
		if (!eleSetBegin && tmpStr[0]=='[')
		{
			eleSetBegin = 1;
			continue;
		}

		// If we found the ']' character, the elephant set had end,
		// and ready to prepare another set.
		//
		if (!eleSetEnd && tmpStr[0]==']')
		{
			eleSetEnd = 1;
			continue;
		}

		/*if (strstr(mapLine, "ELE")) {
			elephant.m_SetElephantId(m_ParseAndGetEleId(mapLine));
			continue;
		}*/
        if (strstr(mapLine, "Hostip")) {

			elephant.SetHost(m_ParseAndGet(mapLine,"Hostip"));
			continue;
		}
		if (strstr(mapLine, "Port")) {

			elephant.SetPort(atoi(m_ParseAndGet(mapLine,"Port").c_str()));
			continue;
		}
		if (strstr(mapLine, "User")) {

			elephant.SetUser(m_ParseAndGet(mapLine,"User"));
			continue;
		}
		if (strstr(mapLine, "Dbname")) {

			elephant.SetDbname(m_ParseAndGet(mapLine,"Dbname"));
			continue;
		}
		if (strstr(mapLine, "Password")) {

			elephant.SetPassword(m_ParseAndGet(mapLine,"Password"));
			continue;
		}
		if (strstr(mapLine, "TruncAfterRep")) {
			
            elephant.m_SetTruncAfter( ("yes"==m_ParseAndGet(mapLine, "TruncAfterRep")) ? true : false );
			continue;
		}
		/*if (strstr(mapLine, "WatchIt")) {

			elephant.m_SetWatchTime(atoi(m_ParseAndGet(mapLine, "WatchIt").c_str()));
			continue;
		}*/
        if (strstr(mapLine, "PrettySQL")) {

            elephant.m_SetPrettySql( ("yes"==m_ParseAndGet(mapLine,"PrettySQL")) ? true : false );
            continue;
        }
		if (strstr(mapLine, "Filename")) {

			elephant.m_SetFileName(m_ParseAndGet(mapLine, "Filename"));
			continue;
		}
        if (strstr(mapLine, "ReportLoc")) {
            
            elephant.m_SetRepLocation(m_ParseAndGet(mapLine, "ReportLoc"));
            continue;
        }
        if (strstr(mapLine, "RepeatRepFor")) {

            elephant.m_SetRepeatRep(m_ParseAndGet(mapLine, "RepeatRepFor"));
            continue;
        }
		if (strstr(mapLine, "FromDate")) {
			elephant.m_SetFromDate(m_ParseAndGet(mapLine, "FromDate"));
			continue;
		}
		if (strstr(mapLine, "ToDate")) {
			elephant.m_SetToDate(m_ParseAndGet(mapLine, "ToDate"));
			continue;
		}
	}

	if (eleSetBegin && eleSetEnd) {
		eleSetBegin = eleSetEnd = 0;
		elephants.push_back(elephant);
	}

	fclose(mapFilePointer);
	return elephants;
}


string mapParser::m_ParseAndGet(const char* line, const char* getMe) {
	char token[20]="";
	char *scanVal,*tmpStr;
	string sendMe="";

	sscanf(line, "%[^:]s", token);
	tmpStr = TrimWhiteSpaces(token);
	if(strcmp(tmpStr, getMe)==0) {
		scanVal = (char *) calloc(sizeof(char), CONF_MAX_ELEMENT_LENGTH);
		sscanf(line, "%*[^:]:%[^\n]", scanVal);
		sendMe[strlen(scanVal)+1] = '\0';
		sendMe.append(scanVal);
        //cout << line << scanVal << endl;
        //delete[] tmpStr;
		free(scanVal);
		tmpStr = NULL;
		scanVal = NULL;
		return TrimWhiteSpaces(sendMe.c_str());
	}

	delete[] tmpStr;
	tmpStr = NULL;
	return " ";
}


char* TrimWhiteSpaces(const char* text)
{
	char* trimedLine;
	int fromPos, toPos, iter, i;
	fromPos = toPos = -1;

	for(iter=0; iter<(signed)strlen(text); iter++)
	{
		if (fromPos==-1 && text[iter]!=' ' && text[iter]!= '\t') {
			fromPos = iter;
			toPos = iter;
		}

		if (toPos!=-1 && text[iter]!=' ' && text[iter]!='\t') {
			toPos = iter;
		}
	}

	trimedLine = new char[ (toPos-fromPos) + 2];

	for (i=0, iter=fromPos; i<=(toPos-fromPos);i++)
		trimedLine[i] = text[iter++];

	 trimedLine[i] = '\0';

	return trimedLine;
}

// Framing current timestamp,
// and returning the string formatted date time.
//

string GetCurrentTimeStamp() {

	stringstream sstr;
	time_t t= time(0);
	sstr 	<< localtime(&t)->tm_mday << "-" \
			<< localtime(&t)->tm_mon << "-"  \
			<< localtime(&t)->tm_year + 1900 << " " \
			<< localtime(&t)->tm_hour \
			<< ":" << localtime(&t)->tm_min  \
			<< ":" << localtime(&t)->tm_sec << " "; \
	return sstr.str();
}

// Converting number to string
//

string ToString(size_t num) {

	stringstream sstr;
	sstr << num;
	return sstr.str();
}

// Converting to Quoted String
//

string ToQuoteString(string str) {

	return "\""+str+"\"";
}
// Converting the pgArray to JsArray
//
string ToJsArray(string value) {

	string jsArray =" ";

	/* pgArray looks like below, and we need to convert to jsArray as looke like below.
	*
	* pgArray
	* -------
	* {"1405949060.46482,13","1405949120.53473,13","1405949180.56567, 13",
	* "1405949600.79559, 13"}
	*
	* jsArray
	* -------
	*
	* [{x:1405949060.46482, y:13},{x:1405949120.53473, y:13},{x:1405949180.56567, y:13},
	* {x:1405949600.79559, y:13}];
	*
	*/
	if (value.empty()) {

		cout << "Null string has passed.";
		return "";
	}
	else {
		bool _1quoteFound, _2quoteFound, commaFound, xStamp, yStamp;
		_1quoteFound = _2quoteFound = commaFound = xStamp = yStamp = false;
		long length = value.length();
		string readValue =" ";

		jsArray+="[";

		for (long c=0; c<length; c++) {

			if (!_1quoteFound && value[c]=='"') {
				_1quoteFound = true;
				jsArray += "{\"x\":";
				continue;
			}

			if (_1quoteFound && ((value[c]>='0' && value[c]<='9') || value[c]=='.')) {
							jsArray += value[c];
							continue;
			}

			if (_1quoteFound && !_2quoteFound && value[c]==',') {
				commaFound = true;
				jsArray += ", \"y\":";
				continue;
			}

			if (_1quoteFound && commaFound && !_2quoteFound && value[c]=='"') {
				_2quoteFound = true;
				jsArray += "}";
				continue;
			}

			// If one set of value "122323.3434, 3434" then reset all the flags
			// to hunt for the next value.
			//
			if (_1quoteFound && commaFound && _2quoteFound && value[c]==',') {

				_1quoteFound = commaFound = _2quoteFound = false;
				jsArray += ",";
				continue;
			}
		}

		jsArray+="]";
	}

	return jsArray;
}

// Checking the string is number or not
//
bool IsNumber(string& str) {

	string::const_iterator it = str.begin();

	while (it!=str.end() && isdigit(*it))
		++it;

	return !str.empty() && it==str.end();
}

// SQL Pretty Print
//
string sqlPrettyPrint(string& query) {

    stringstream str;
    string tagCodeOprts="<span class=\"keyword oprnd\"> $1 </span> \
                        <span class=\"keyword opert\"> $2 </span> \
                        <span class=\"keyword oprnd\"> $3 </span>";
    string tagCodeRes="<span class=\"keyword reserved\"> $1 </span>";
    string tagCodeUnRes="<span class=\"keyword unreserved\"> $1 </span>";
    
    /*str << regex_replace(query, rgxOprts, tagCodeOprts);
    query=str.str();
    str.str(string());
    str.clear();
    */
    str << regex_replace(query, rgxRsrvdKeyWords, tagCodeRes);
    query=str.str();
    str.str(string());
    str.clear();

    str << regex_replace(query, rgxUnrsrvdKeyWords, tagCodeUnRes);
    return str.str();
}
