/*
 * hk_reportHtml.cpp
 *
 *  Created on: Jul 20, 2014
 *      Author: dinesh
 * Description:
 *
 * 				hk_reportHtml.h implementation.
 */

#include "../include/hk_reportHtml.h"


hk_tablechart_pref::hk_tablechart_pref(bool isEnableSort, bool isEnablePrettyPrint, bool isEnableFiltered) {
    
    m_isEnableSort = isEnableSort;
    m_isEnablePrettyPrint = isEnablePrettyPrint;
    m_isEnableFiltered = isEnableFiltered;
}


bool hk_tablechart_pref::m_getIsEnableFiltered() {

    return m_isEnableFiltered;
}

bool hk_tablechart_pref::m_getIsEnablePrettyPrint() {
    
    return m_isEnablePrettyPrint;
}

bool hk_tablechart_pref::m_getIsEnableSort() {
    
    return m_isEnableSort;
}

hk_tablechart_pref::~hk_tablechart_pref() {
    
}


hk_linechart_pref::hk_linechart_pref(string chartXLabel, string chartYLabel, bool isFocusOn) {
    
    m_chartXLabel = chartXLabel;
    m_chartYLabel = chartYLabel;
    m_isFocusOn = isFocusOn;
}

string hk_linechart_pref::m_getChartXLabel() {
    
    return m_chartXLabel;
}

string hk_linechart_pref::m_getChartYLabel() {
    
    return m_chartYLabel;
}

bool hk_linechart_pref::m_getIsFocusOn() {
    
    return m_isFocusOn;
}

hk_linechart_pref::~hk_linechart_pref() {
    
}

// Constructor for Table Type
//
hk_chart_pref::hk_chart_pref(string chartHeader, string chartUIKITSize, bool isEnableSort, bool isEnablePrettyPrint, bool isEnableFiltered)
:  hk_tablechart_pref(isEnableSort, isEnablePrettyPrint, isEnableFiltered) {

    m_chartHeader = chartHeader;
    m_chartUIKITSize = chartUIKITSize;
}

// Constructor for Line Type
//
hk_chart_pref::hk_chart_pref(string chartHeader, string chartUIKITSize, string chartXLabel, string chartYLabel, bool isFocusOn)
: hk_linechart_pref(chartXLabel,  chartYLabel,  isFocusOn) {
    m_chartHeader = chartHeader;
    m_chartUIKITSize = chartUIKITSize;
}

string hk_chart_pref::m_getChartHeader() {
    
    return m_chartHeader;
}

string hk_chart_pref::m_getChartUIKITSize() {
    
    return m_chartUIKITSize;
}

hk_chart::hk_chart() {
    // Setting all the default chart options
    //
    m_chartType = UNKOWN_CHART;
    m_chartLevel = UNKNOWN_LEVEL;
}

hk_chart::hk_chart(HK_CHART_LEVEL level, HK_CHART_TYPES ctype, hk_chart_pref chartPref) {
    
    m_chartType = ctype;
    m_chartLevel = level;
    m_chart = chartPref;
}

HK_CHART_TYPES hk_chart::m_getChartType() {
    
    return m_chartType;
}

HK_CHART_LEVEL hk_chart::m_getChartLevel() {
    
    return m_chartLevel;
}

hk_chart_pref hk_chart::m_getChart() {
    
    return m_chart;
}


hk_reportQueries::hk_reportQueries(HK_CHART_LEVEL level, string query, bool isDbLevel, HK_CHART_TYPES ctype, hk_chart_pref chartPref):
hk_chart(level, ctype, chartPref){
    
    m_query = query;
    m_isDbLevel = isDbLevel;
}


hk_dbinfo::hk_dbinfo() {
    
    string trunc_table_list = "snapshots.snap, snapshots.snap_databases, snapshots.snap_indexes, snapshots.snap_stat_activity, snapshots.snap_user_tables, snapshots.snap_xlog_location";
    m_truncQuery = "TRUNCATE " + trunc_table_list;
    
    // Listing Snaps
    //
    
    m_reportQueries.push_back(hk_reportQueries(DATABASE_LEVEL,
                                               " SELECT snaps_id AS \"Snap ID\","
                                               " dttm AS \"Timestamp\""
                                               " FROM snapshots.list_snaps() WHERE TO_TIMESTAMP(TO_CHAR(dttm,'DD-MON-YYYY HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS')"
                                               " BETWEEN $1::TIMESTAMP AND $2::TIMESTAMP",
                                               false,
                                               TABLE_CHART,
                                               hk_chart_pref("Snaps", "uk-width-large-1-3", true, false, false)
                                               ));
                                               
    // Cpu Information
    //
    m_reportQueries.push_back(hk_reportQueries(OS_LEVEL,
                                               " WITH snap_cpu AS("
                                               " SELECT snap_id, dttm, systime, usertime, idletime, waittime FROM snapshots.snap_cpu"
                                               " WHERE TO_TIMESTAMP(TO_CHAR(dttm,'DD-MON-YYYY HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS')"
                                               " BETWEEN $1::TIMESTAMP AND $2::TIMESTAMP"
                                               " )"
                                               " SELECT cmd, array_agg(pickup_time||','||val ORDER BY pickup_time)"
                                               " FROM"
                                               " ("
                                               " SELECT 'System Time' cmd, EXTRACT(EPOCH FROM dttm) pickup_time, SUM(systime) val FROM snap_cpu GROUP BY cmd, pickup_time"
                                               " UNION"
                                               " SELECT 'User Time' cmd , EXTRACT(EPOCH FROM dttm) pickup_time, SUM(usertime) val FROM snap_cpu  GROUP BY cmd,pickup_time"
                                               " UNION"
                                               " SELECT 'Idle Time' cmd, EXTRACT(EPOCH FROM dttm) pickup_time, SUM(idletime) val FROM snap_cpu GROUP BY cmd, pickup_time"
                                               " UNION"
                                               " SELECT 'Wait Time' cmd, EXTRACT(EPOCH FROM dttm) pickup_time, SUM(waittime) val FROM snap_cpu GROUP BY cmd, pickup_time"
                                               " ) as foo"
                                               " GROUP BY 1;",
                                               false,
                                               LINE_CHART,
                                               hk_chart_pref("CPU Usage", "uk-width-large-9-10", "Time", "Used", true)
                                               ));
    
    
    // Memory Information
    //
    m_reportQueries.push_back(hk_reportQueries(OS_LEVEL,
                                               " WITH snap_mem_activity AS("
                                               " SELECT * FROM snapshots.snap_mem"
                                               " WHERE TO_TIMESTAMP(TO_CHAR(dttm,'DD-MON-YYYY HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS')"
                                               " BETWEEN $1::TIMESTAMP AND $2::TIMESTAMP"
                                               " )"
                                               " SELECT cmd, array_agg(pickup_time||','||val ORDER BY pickup_time)"
                                               " FROM"
                                               " ("
                                               " SELECT 'Memory' cmd, EXTRACT(EPOCH FROM dttm) pickup_time, SUM(used) val FROM snap_mem_activity WHERE metric ~* 'mem' GROUP BY cmd, pickup_time"
                                               " UNION"
                                               " SELECT 'Swap' cmd , EXTRACT(EPOCH FROM dttm) pickup_time, SUM(used) val FROM snap_mem_activity WHERE metric ~* 'swap' GROUP BY cmd,pickup_time"
                                               " UNION"
                                               " SELECT 'Buffers/Cache' cmd, EXTRACT(EPOCH FROM dttm) pickup_time, COUNT(1) val FROM snap_mem_activity WHERE metric ~* 'buffers' GROUP BY cmd, pickup_time"
                                               " ) as foo"
                                               " GROUP BY 1;",
                                               false,
                                               LINE_CHART,
                                               hk_chart_pref("Memory Used Overview", "uk-width-large-9-10", "Time", "Used", true)
                                               ));
    
    
    // FileSystem I/O Information
    //
    m_reportQueries.push_back(hk_reportQueries(OS_LEVEL,
                                               " WITH snap_io_activity AS("
                                               " SELECT * FROM snapshots.snap_iostat"
                                               " WHERE TO_TIMESTAMP(TO_CHAR(dttm,'DD-MON-YYYY HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS')"
                                               " BETWEEN $1::TIMESTAMP AND $2::TIMESTAMP"
                                               " )"
                                               " SELECT cmd, array_agg(pickup_time||','||val ORDER BY pickup_time)"
                                               " FROM"
                                               " ("
                                               " SELECT 'Reads :'||filesystem cmd, EXTRACT(EPOCH FROM dttm) pickup_time, SUM(rbytes)::bigint>>20 val FROM snap_io_activity GROUP BY cmd, pickup_time"
                                               " UNION"
                                               " SELECT 'Writes :'||filesystem cmd, EXTRACT(EPOCH FROM dttm) pickup_time, SUM(wbytes)::bigint>>20 val FROM snap_io_activity GROUP BY cmd, pickup_time"
                                               " ) as foo"
                                               " GROUP BY 1;",
                                               false,
                                               LINE_CHART,
                                               hk_chart_pref("Filesystem Overview", "uk-width-large-9-10", "Time", "Used", true)
                                               ));
    
    // Connections Overview
    //
    m_reportQueries.push_back(hk_reportQueries(CLUSTER_LEVEL,
                                               " WITH snap_stat_activity AS("
                                               " SELECT * FROM snapshots.snap_stat_activity"
                                               " WHERE TO_TIMESTAMP(TO_CHAR(dttm,'DD-MON-YYYY HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS')"
                                               " BETWEEN $1::TIMESTAMP AND $2::TIMESTAMP"
                                               " )"
                                               " SELECT cmd, array_agg(pickup_time||','||val ORDER BY pickup_time)"
                                               " FROM"
                                               " ("
                                               " SELECT 'Active' cmd, EXTRACT(EPOCH FROM dttm) pickup_time, COUNT(1) val FROM snap_stat_activity WHERE query !~* 'idle' GROUP BY cmd, pickup_time"
                                               " UNION"
                                               " SELECT 'Idle' cmd , EXTRACT(EPOCH FROM dttm) pickup_time, COUNT(1) val FROM snap_stat_activity WHERE query ~* 'idle' GROUP BY cmd,pickup_time"
                                               " UNION"
                                               " SELECT 'Idle in Trx' cmd, EXTRACT(EPOCH FROM dttm) pickup_time, COUNT(1) val FROM snap_stat_activity WHERE query ~* 'idle in transaction' GROUP BY cmd, pickup_time"
                                               " UNION"
                                               " SELECT 'Total Conn' cmd, EXTRACT(EPOCH FROM dttm) pickup_time, COUNT(1) val FROM snap_stat_activity GROUP BY cmd, pickup_time"
                                               " ) as foo"
                                               " GROUP BY 1;",
                                               false,
                                               LINE_CHART,
                                               hk_chart_pref("Connections Overview", "uk-width-large-9-10", "Time", "Total(#)", true)
                                               ));
    
    // Live (vs) Dead Tuples
    //
    m_reportQueries.push_back(hk_reportQueries(DATABASE_LEVEL,
                                               " SELECT"
                                               " cmd, array_agg(pickup_time||','||val ORDER BY pickup_time)"
                                               " FROM"
                                               " ("
                                               " WITH snaps as ("
                                               " SELECT"
                                               " snap_id, SUM(n_live_tup) as n_live_tup, SUM(n_dead_tup) as n_dead_tup"
                                               " FROM"
                                               " snapshots.snap_user_tables"
                                               " WHERE TO_TIMESTAMP(TO_CHAR(dttm,'DD-MON-YYYY HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS')"
                                               " BETWEEN $1::TIMESTAMP AND $2::TIMESTAMP"
                                               " GROUP BY snap_id"
                                               " ORDER BY 1)"
                                               " SELECT  'Live Tuples' cmd, EXTRACT(EPOCH FROM dttm)::BIGINT pickup_time, s2.n_live_tup-s1.n_live_tup val"
                                               " FROM"
                                               " snaps s1, snaps s2, snapshots.snap"
                                               " WHERE   s1.snap_id +1 = s2.snap_id AND s2.snap_id=snap.snap_id"
                                               " UNION"
                                               " SELECT  'Dead Tuples' cmd, EXTRACT(EPOCH FROM dttm)::BIGINT pickup_time, s2.n_dead_tup-s1.n_dead_tup val"
                                               " FROM"
                                               " snaps s1, snaps s2, snapshots.snap"
                                               " WHERE   s1.snap_id +1 = s2.snap_id AND s2.snap_id=snap.snap_id"
                                               " ) as foo"
                                               " GROUP BY cmd;",
                                               false,
                                               LINE_CHART,
                                               hk_chart_pref("Live (Vs) Dead Tuples", "uk-width-large-9-10", "Time", "Total(#)", true)
                                               ));
    
    // Scans Overview
    //
    m_reportQueries.push_back(hk_reportQueries(DATABASE_LEVEL,
                                               " SELECT"
                                               " cmd, array_agg(pickup_time||','||val ORDER BY pickup_time)"
                                               " FROM"
                                               " ("
                                               " WITH snaps as ("
                                               " SELECT"
                                               " snap_id, SUM(seq_scan) as seq_scan, SUM(idx_scan) as idx_scan"
                                               " FROM"
                                               " snapshots.snap_user_tables"
                                               " WHERE TO_TIMESTAMP(TO_CHAR(dttm,'DD-MON-YYYY HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS')"
                                               " BETWEEN $1::TIMESTAMP AND $2::TIMESTAMP"
                                               " GROUP BY snap_id"
                                               " ORDER BY 1)"
                                               " SELECT  'Sequential Scans' cmd, EXTRACT(EPOCH FROM dttm)::BIGINT pickup_time, s2.seq_scan-s1.seq_scan val"
                                               " FROM"
                                               " snaps s1, snaps s2, snapshots.snap"
                                               " WHERE   s1.snap_id +1 = s2.snap_id AND s2.snap_id=snap.snap_id"
                                               " UNION"
                                               " SELECT  'Index Scans' cmd, EXTRACT(EPOCH FROM dttm)::BIGINT pickup_time, s2.idx_scan-s1.idx_scan val"
                                               " FROM"
                                               " snaps s1, snaps s2, snapshots.snap"
                                               " WHERE   s1.snap_id +1 = s2.snap_id AND s2.snap_id=snap.snap_id"
                                               " ) as foo"
                                               " GROUP BY cmd;",
                                               false,
                                               LINE_CHART,
                                               hk_chart_pref("Scans Overview", "uk-width-large-9-10", "Time", "Total(#)", true)
                                               ));
    
    // Database Growth
    //
    m_reportQueries.push_back(hk_reportQueries(CLUSTER_LEVEL,
                                               " SELECT"
                                               " s2.datname as cmd, array_agg(EXTRACT(EPOCH FROM s2.dttm)::BIGINT||','||ABS(s2.dbsize-s1.dbsize)/1024/1024 ORDER BY s2.dttm)"
                                               " FROM"
                                               " snapshots.snap_databases s1, snapshots.snap_databases s2"
                                               " WHERE s1.snap_id=s2.snap_id+1 AND s1.datname=s2.datname AND s2.datname !~ '^template'"
                                               " AND TO_TIMESTAMP(TO_CHAR(s1.dttm,'DD-MON-YYYY HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS')"
                                               " BETWEEN $1::TIMESTAMP AND $2::TIMESTAMP"
                                               " GROUP BY s2.datname;",
                                               false,
                                               LINE_CHART,
                                               hk_chart_pref("Database Growth", "uk-width-large-9-10", "Time", "Size (MB)", true)
                                               ));
    
    // Database Hit Ratio
    //
    m_reportQueries.push_back(hk_reportQueries(CLUSTER_LEVEL,
                                               " SELECT"
                                               " s2.datname as cmd, array_agg(EXTRACT(EPOCH FROM s2.dttm)::BIGINT||','||TRUNC(((blks_hit)/(blks_read+blks_hit)::numeric)*100, 2) ORDER BY s2.dttm)"
                                               " FROM"
                                               " snapshots.snap_databases s2"
                                               " WHERE s2.datname !~ '^template' AND (blks_read+blks_hit)!=0"
                                               " AND TO_TIMESTAMP(TO_CHAR(dttm,'DD-MON-YYYY HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS')"
                                               " BETWEEN $1::TIMESTAMP AND $2::TIMESTAMP"
                                               " GROUP BY s2.datname;",
                                               false,
                                               LINE_CHART,
                                               hk_chart_pref("Database Hit Ratio", "uk-width-large-9-10", "Time", "Percentage (%)", true)
                                               ));
    
    
    // Transaction Commit/Rollback
    //
    m_reportQueries.push_back(hk_reportQueries(CLUSTER_LEVEL,
                                               " SELECT"
                                               " cmd, array_agg(pickup_time||','||val ORDER BY pickup_time)"
                                               " FROM"
                                               " ("
                                               " WITH snaps as ("
                                               " SELECT"
                                               " snap_id, SUM(xact_commit) as total_xact_cmt, SUM(xact_rollback) as total_xact_rollback"
                                               " FROM"
                                               " snapshots.snap_databases"
                                               " WHERE TO_TIMESTAMP(TO_CHAR(dttm,'DD-MON-YYYY HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS')"
                                               " BETWEEN $1::TIMESTAMP AND $2::TIMESTAMP"
                                               " GROUP BY snap_id"
                                               " ORDER BY 1)"
                                               " SELECT  'Commit' cmd, EXTRACT(EPOCH FROM dttm)::BIGINT pickup_time, s2.total_xact_cmt-s1.total_xact_cmt val"
                                               " FROM"
                                               " snaps s1, snaps s2, snapshots.snap"
                                               " WHERE   s1.snap_id +1 = s2.snap_id AND s2.snap_id=snap.snap_id"
                                               " UNION"
                                               " SELECT  'Rollback' cmd, EXTRACT(EPOCH FROM dttm)::BIGINT pickup_time, s2.total_xact_rollback-s1.total_xact_rollback"
                                               " FROM"
                                               " snaps s1, snaps s2, snapshots.snap"
                                               " WHERE   s1.snap_id +1 = s2.snap_id AND s2.snap_id=snap.snap_id"
                                               " ) as foo"
                                               " GROUP BY cmd;",
                                               false,
                                               LINE_CHART,
                                               hk_chart_pref("Transactions Overview", "uk-width-large-9-10", "Time", "Count (#)", true)
                                               ));
    // DML Activities
    //
    m_reportQueries.push_back(hk_reportQueries(DATABASE_LEVEL,
                                               " SELECT"
                                               " cmd, array_agg(pickup_time||','||val ORDER BY pickup_time)"
                                               " FROM"
                                               " ("
                                               " WITH snaps as ("
                                               " SELECT"
                                               " snap_id, SUM(n_tup_ins) as n_tup_ins, SUM(n_tup_upd) as n_tup_upd, SUM(n_tup_hot_upd) as n_tup_hot_upd, SUM(n_tup_del) as n_tup_del"
                                               " FROM"
                                               " snapshots.snap_user_tables"
                                               " WHERE TO_TIMESTAMP(TO_CHAR(dttm,'DD-MON-YYYY HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS')"
                                               " BETWEEN $1::TIMESTAMP AND $2::TIMESTAMP"
                                               " GROUP BY snap_id"
                                               " ORDER BY 1)"
                                               " SELECT  'Insert' cmd, EXTRACT(EPOCH FROM dttm)::BIGINT pickup_time, s2.n_tup_ins-s1.n_tup_ins val"
                                               " FROM"
                                               " snaps s1, snaps s2, snapshots.snap"
                                               " WHERE   s1.snap_id +1 = s2.snap_id AND s2.snap_id=snap.snap_id"
                                               " UNION"
                                               " SELECT  'Update' cmd, EXTRACT(EPOCH FROM dttm)::BIGINT pickup_time, s2.n_tup_upd-s1.n_tup_upd val"
                                               " FROM"
                                               " snaps s1, snaps s2, snapshots.snap"
                                               " WHERE   s1.snap_id +1 = s2.snap_id AND s2.snap_id=snap.snap_id"
                                               " UNION"
                                               " SELECT  'Hot Update' cmd, EXTRACT(EPOCH FROM dttm)::BIGINT pickup_time, s2.n_tup_hot_upd-s1.n_tup_hot_upd val"
                                               " FROM"
                                               " snaps s1, snaps s2, snapshots.snap"
                                               " WHERE   s1.snap_id +1 = s2.snap_id AND s2.snap_id=snap.snap_id"
                                               " UNION"
                                               " SELECT  'Delete' cmd, EXTRACT(EPOCH FROM dttm)::BIGINT pickup_time, s2.n_tup_del-s1.n_tup_del val"
                                               " FROM"
                                               " snaps s1, snaps s2, snapshots.snap"
                                               " WHERE   s1.snap_id +1 = s2.snap_id AND s2.snap_id=snap.snap_id"
                                               " ) as foo"
                                               " GROUP BY cmd;",
                                               false,
                                               LINE_CHART,
                                               hk_chart_pref("DML Activities", "uk-width-large-9-10", "Time", "Total (#)", true)
                                               ));
    
    // Connections Overview In Table Chart.
    //
    m_reportQueries.push_back(hk_reportQueries(CLUSTER_LEVEL,
                                               " WITH snap AS ("
                                               " SELECT * FROM snapshots.snap_stat_activity"
                                               " WHERE TO_TIMESTAMP(TO_CHAR(dttm,'DD-MON-YYYY HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS')"
                                               " BETWEEN $1::TIMESTAMP AND $2::TIMESTAMP)"
                                               " SELECT client_addr AS \"Host IP\", usename AS \"Username\", datname AS \"Database\", COUNT(*) AS \"Total\""
                                               " FROM snap GROUP BY 1, 2, 3 HAVING client_addr IS NOT NULL"
                                               " UNION ALL"
                                               " SELECT client_addr, usename, NULL,  COUNT(*) FROM snap GROUP BY 1,2 HAVING client_addr IS NOT NULL"
                                               " UNION ALL"
                                               " SELECT client_addr, NULL, NULL, COUNT(*) FROM snap GROUP BY 1 HAVING client_addr IS NOT NULL"
                                               " UNION ALL"
                                               " SELECT NULL, NULL, NULL, COUNT(*) FROM snap WHERE client_addr IS NOT NULL"
                                               " ORDER BY 1,2,3;",
                                               false,
                                               TABLE_CHART,
                                               hk_chart_pref("Connections Summary", "uk-width-large-3-4", true, false, false)
                                               ));
    // Table Metrics
    //
    m_reportQueries.push_back(hk_reportQueries(DATABASE_LEVEL,
                                               " select snap_id AS \"Snap ID\", dttm AS \"TimeStamp\", schemaname AS \"SchemaName\","
                                               " relname AS \"RelName\", n_tup_ins AS \"Insert\","
                                               " COALESCE(n_tup_ins - lag ( n_tup_ins ) over ( order by snap_id ), 0) AS \"InsPer5\","
                                               " n_tup_upd AS \"Update\","
                                               " COALESCE(n_tup_upd - lag ( n_tup_upd ) over ( order by snap_id ), 0) AS \"UpdPer5\","
                                               " n_tup_del AS \"Delete\","
                                               " COALESCE(n_tup_del - lag ( n_tup_del ) over ( order by snap_id ), 0) AS \"DelPer5\","
                                               " pg_size_pretty (totalrelsize) AS \"Table Size\""
                                               " FROM snapshots.snap_user_tables"
                                               " WHERE TO_TIMESTAMP(TO_CHAR(dttm,'DD-MON-YYYY HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS')"
                                               " BETWEEN $1::TIMESTAMP AND $2::TIMESTAMP LIMIT 20",
                                               false,
                                               TABLE_CHART,
                                               hk_chart_pref("Table Stat Activity", "uk-width-large-3-4", true, false, false)
                                               ));
    
    // Top 15 Big Tables
    //
    m_reportQueries.push_back(hk_reportQueries(DATABASE_LEVEL,
                                               " SELECT schemaname as \"Schema\", relname as \"Table\", max(n_live_tup) as \"Tuples\", "
                                               " max(relsize)/1024/1024 as \"Relation Size (MB)\", max(totalrelsize)/1024/1024 as \"Total Relation Size (MB)\""
                                               " FROM snapshots.snap_user_tables"
                                               " WHERE TO_TIMESTAMP(TO_CHAR(dttm,'DD-MON-YYYY HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS')"
                                               " BETWEEN $1::TIMESTAMP AND $2::TIMESTAMP"
                                               " GROUP BY 1, 2 HAVING max(n_live_tup)>0 ORDER BY 4 DESC LIMIT 15;",
                                               false,
                                               TABLE_CHART,
                                               hk_chart_pref("Top 15 Big Tables", "uk-width-large-3-4", true, false, false)
                                               ));
    
    // Hot Tables
    //
    m_reportQueries.push_back(hk_reportQueries(DATABASE_LEVEL,
                                               " SELECT schemaname as \"Schema\", relname as \"Table\", sum(n_tup_ins + n_tup_upd + n_tup_del) as \"No_Of_Activities\" "
                                               " FROM snapshots.snap_user_tables "
                                               " WHERE TO_TIMESTAMP(TO_CHAR(dttm,'DD-MON-YYYY HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS')"
                                               " BETWEEN $1::TIMESTAMP AND $2::TIMESTAMP"
                                               " GROUP BY 1,2 HAVING sum(n_tup_ins + n_tup_upd + n_tup_del) >0 "
                                               " ORDER BY 3 DESC LIMIT 50;",
                                               false,
                                               TABLE_CHART,
                                               hk_chart_pref("Top 50 Hot Tables", "uk-width-large-3-4", true, false, false)
                                               ));
    
    // Queries.
    //
    m_reportQueries.push_back(hk_reportQueries(CLUSTER_LEVEL,
                                               " SELECT date_trunc('hour',dttm)::timestamp \"Time\","
                                               " COALESCE(client_addr::text, 'localhost') \"Host\","
                                               " datname \"Database\","
                                               " COUNT(*) \"Count\","
                                               " regexp_replace(query,'\n|<|>',' ','g') \"Query\""
                                               " FROM snapshots.snap_stat_activity "
                                               " WHERE TO_TIMESTAMP(TO_CHAR(dttm,'DD-MON-YYYY HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS')"
                                               " BETWEEN $1::TIMESTAMP AND $2::TIMESTAMP AND TRIM(query)!='' AND TRIM(query) IS NOT NULL AND query !~* '<idle>'"
                                               " GROUP BY 1,2,3,5 ORDER BY 3 DESC LIMIT 100;",
                                               false,
                                               TABLE_CHART,
                                               hk_chart_pref("Ran Queries", "uk-width-large-3-4", true, false, false)
                                               ));
    // Autovacuum, Vacuum activities.
    //
    m_reportQueries.push_back(hk_reportQueries(DATABASE_LEVEL,
                                               " SELECT MAX(vacuum_count) \"Vacuum_Count\", MAX(autovacuum_count) \"Autovacuum_Count\","
                                               " MAX(analyze_count) \"Analyze_Count\", MAX("
                                               " autoanalyze_count) \"Autoanalyze_count\""
                                               " FROM"
                                               " snapshots.snap_user_tables"
                                               " WHERE TO_TIMESTAMP(TO_CHAR(dttm,'DD-MON-YYYY HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS')"
                                               " BETWEEN $1::TIMESTAMP AND $2::TIMESTAMP",
                                               false,
                                               TABLE_CHART,
                                               hk_chart_pref("Vacuum Activity", "uk-width-large-3-4", true, false, false)
                                               ));
    
    
}

string hk_dbinfo::m_getTruncQuery() {
    
    return m_truncQuery;
}
string hk_reportHtml::m_execGetBarChartVal(PGconn* conn, string query) {
    
    string result="";
    PGresult *res = PQexec(conn, query.c_str());
    
    // To generate NVD3 Bar graphs, we have to use below JSON formatted text.
    // Hence, converting the pg result into JSON format, to get the desired BAR Chart.
    //
    // Syntax:
    //
    // {"data" : [ {"label": "postgres", "value": 200}, {"label": "production", "value": 500} ] }
    //
    for(int i=0; i<PQntuples(res); i++) {
        
        if(i)
            result += " , ";
        
        result += "{ \"label\": " + (string)PQgetvalue(res, i, 0) + " , \"value\": " + (string)PQgetvalue(res, i, 1) + "}" ;
    }
    
    result = "{\"data\": [" + result + "]}";
    return result;
}

vector<hk_reportQueries> hk_dbinfo::m_getReportQueries() {
    
    return m_reportQueries;
}

void hk_reportHtml::m_execGetHtmlTable(PGconn* conn, string instruct, string* params, bool isCubeTable, bool isPrettySql) {
    
    string htmlTable="";
    string columnHeader="";
    bool isCubeAggVal = false;
    const char* param_vals[2];
    param_vals[0]=params[0].c_str();
    param_vals[1]=params[1].c_str();
    PGresult* res = PQexecParams(conn, instruct.c_str(), 2, NULL, param_vals, NULL, NULL, 0);
    
    for (int i=0; i<PQntuples(res); i++) {
        
        for (int j=0; j<PQnfields(res); j++) {
            
            if (i==0) {
                columnHeader += "<th>" + string(PQfname(res, j)) +"</th>";
            }
            
            if (j==0) m_openTag("tr");
            string result = PQgetvalue(res, i, j);
            
            if (isCubeTable && IsNumber(result) && isCubeAggVal) {
                //htmlTable += "<td class=\"cube-agg-val\" align=\"right\">" + result +"</td>";
                m_openTag("td", "class=\"cube-agg-val\" align=\"right\"");
                m_putTextSource(result);
                m_closeTag();
                isCubeAggVal = false;
            }
            else if(IsNumber(result)){
                //htmlTable += "<td class=\"uk-width-1-10 uk-text-right\">" + result +"</td>";
                m_openTag("td", "class=\"uk-width-1-10 uk-text-right\"");
                m_putTextSource(result);
                m_closeTag();
            }
            else if(result.length()>1) {
                
                if (string(PQfname(res, j)).compare("Query")==0) {
                    //(htmlTable += "<td class=\"uk-width-1-2 uk-text-left\">" + sqlPrettyPrint(result))
                    
                    m_openTag("td", "class=\"uk-width-1-2 uk-text-left\"");
                    m_putTextSource((isPrettySql ? sqlPrettyPrint(result) : result));
                    m_closeTag();
                    
                }
                else {
                    
                    m_openTag("td", "class=\"uk-width-2-10 uk-text-left\"");
                    m_putTextSource(result);
                    m_closeTag();

                }
            }
            // Handling the cube like output.
            //
            // Change the table cell background, if the column is a aggregate result.
            //
            else if(isCubeTable && result.length()<1) {
                string result = PQgetvalue(res, i, PQnfields(res)-1);
                if (IsNumber(result))
                    isCubeAggVal = true;
                
                m_openTag("td", "class=\"uk-width-2-10 uk-text-left\"");
                m_putTextSource("SubTotal"+string(PQfname(res, j)));
                m_closeTag();
            }
            if (j+1==PQnfields(res)) m_closeTag();
        }
        
        //htmlTable += "</tr>";
        //m_closeTag();
    }
    
    m_openTag("thead");
    m_putTextSource(columnHeader);
    m_closeTag();

    PQclear(res);
    res = NULL;
}

string hk_reportHtml::m_execGetJSONArray(PGconn* conn, string instruct, string* params) {
    
    string jsonArray;
    string data, lables;
    const char* param_vals[2];
    
    param_vals[0]=params[0].c_str();
    param_vals[1]=params[1].c_str();
    PGresult* res = PQexecParams(conn, instruct.c_str(),2, NULL, param_vals, NULL, NULL, 0);
    
    for (int i=0; i<PQntuples(res); i++) {
        
        if (i) {
            
            data += " , ";
            lables += " , ";
        }
        
        for (int j=0; j<PQnfields(res); j++) {
            
            string result;
            result = PQgetvalue(res, i, j);
            if (j==1)
                data += ToJsArray(result);
            else
                lables += ToQuoteString(result);
        }
    }
    
    data = "[" + data + "]";
    lables = "[" + lables + "]";
    
    jsonArray = "{\"data\":" + data + "," +
				"\"lables\":" + lables + "}";
    
    PQclear(res);
    res = NULL;
    return jsonArray;
}

hk_reportHtml::hk_reportHtml(string rfile) {
    
    m_reportFile.open(rfile.c_str(), ios::out);
    
    if (m_reportFile.is_open())
        m_isReportFileOpened = true;
    else
        m_isReportFileOpened = false;
    
    m_totalLineCharts = 0;
    m_totalTableCharts = 0;
    
    /*
     *  Creating a thread, which write report contents to file
     *  for every 10 ms.
     */
    thread reptWriter(hk_pushCntToFile(), ref(m_reportFile),ref(m_reportContent), ref(m_lock));
    reptWriter.detach();
}


void hk_reportHtml::m_pushCntToStream(const string str) {
    
    m_lock.lock();
    m_reportContent << str << endl;
    m_lock.unlock();
}

void hk_reportHtml::m_writeHtmlHeader() {
    
    m_pushCntToStream("<!DOCTYPE html>");
    m_openTag("html");
    m_openTag("meta", "http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"");
    m_openTag("meta", "charset=\"utf-8\"");
}

void hk_reportHtml::m_putComment(string msg) {
    
    m_pushCntToStream( "<!-- pgHawk Comment: " + msg + " -->");
}

void hk_reportHtml::m_openTag(const string& tag, string attribs) {
    
    m_tags.push_back(tag);
    m_pushCntToStream( "<" + tag + " " + attribs + ">" );
}

void hk_reportHtml::m_putTextSource(const string& cnt) {
    
    m_pushCntToStream(cnt);
}

void hk_reportHtml::m_closeTag() {
    
    m_pushCntToStream( "</" + m_tags.back() + ">" );
    m_tags.pop_back();
}

void hk_reportHtml::m_endHtmlHeader() {
    
    m_pushCntToStream("</html>") ;
}

void hk_reportHtml::m_clearReportContent() {
    
    m_reportContent.str(string());
    m_reportContent.clear();
}

void hk_reportHtml::m_openCData() {
    
    m_pushCntToStream("<![CDATA[");
}

void hk_reportHtml::m_closeCData() {
    
    m_pushCntToStream( "]]>");
}

void hk_reportHtml::m_embeddScriptFileWithParams(string file, vector<size_t> params) {
    
    ifstream scriptFile;
    string sourceLine;
    
    scriptFile.open(file.c_str(), ios::in);
    
    if (scriptFile.is_open()) {
        
        m_openTag("script");
        while (getline(scriptFile, sourceLine)) {
            
            size_t pos = 0;
            for(size_t i=0; i<params.size(); i++) {
                
                pos = sourceLine.find("$"+ToString(i+1));
                if (pos!=string::npos)
                    sourceLine.replace(pos, string("$"+ToString(i+1)).length(), ToString(params[i]));
            }
            m_pushCntToStream(sourceLine);
        }
        m_closeTag();
    }
    
    // Closing the file after loading the contents.
    //
    scriptFile.close();
}

void hk_reportHtml::m_embeddScriptFiles(string* files) {
    
    size_t i = 0;
    ifstream scriptFile;
    string sourceLine;
    while (i<17) {
        
        scriptFile.open(string(files[i].c_str()), ios::in);
        if (scriptFile.is_open()) {
            m_openTag("script");
            while (getline(scriptFile, sourceLine)) {
                
                m_pushCntToStream(sourceLine);
            }
            scriptFile.close();
            m_closeTag();
        }
        i++;
    }
}

void hk_reportHtml::m_embeddCssFiles(string* files) {
    
    size_t i = 0;
    ifstream scriptFile;
    string sourceLine;
    
    while (i<3) {
        m_openTag("style");
        scriptFile.open(string(files[i].c_str()), ios::in);
        if (scriptFile.is_open()) {
            while (getline(scriptFile, sourceLine)) {
                m_pushCntToStream (sourceLine);
            }
            scriptFile.close();
            m_closeTag();
        }
        i++;
    }
}

void hk_reportHtml::m_embeddRequiredScripts() {
    
    string cssFiles[]= {
        "nvd3/nv.d3.css",
        "web/uikit/css/uikit.css",
        "web/pgHawk.css"};
    
    string jsFiles[]= {
        "nvd3/lib/d3.v3.js",
        "nvd3/nv.d3.js",
        "nvd3/src/tooltip.js",
        "nvd3/src/utils.js",
        "nvd3/src/interactiveLayer.js",
        "nvd3/src/models/legend.js",
        "nvd3/src/models/axis.js",
        "nvd3/src/models/scatter.js",
        "nvd3/src/models/line.js",
        "nvd3/src/models/historicalBar.js",
        "nvd3/src/models/lineWithFocusChart.js",
        "nvd3/src/models/pieChart.js",
        "nvd3/src/models/pie.js",
        "nvd3/lib/stream_layer.js",
        "web/tablesort.min.js",
        "web/jquery.js",
        "web/uikit/js/uikit.js"
    };
    
    m_embeddCssFiles(cssFiles);
    m_embeddScriptFiles(jsFiles);
}

bool hk_reportHtml::m_fulshRptCntToFile() {
    
    m_lock.lock();
    m_reportFile << m_reportContent.str();
    m_reportFile.close();
    m_clearReportContent();
    m_lock.unlock();
    return true;
}

void hk_reportHtml::m_appendTitleOrOpts(string title, bool isLine=true) {
    
    m_openTag("div", "class=\"uk-badge uk-badge-danger\"");
    m_putTextSource(title);
    m_closeTag();
    
    if (isLine) {
        m_openTag("div", "class=\"uk-badge uk-badge-notification\"");
        m_openTag("span", "class=\"chart_opts\" chartid="+ToString(m_getTotalLineCharts()));
        m_putTextSource("Zoom");
        m_closeTag();
        m_closeTag();
        
        m_openTag("div", "class=\"uk-badge uk-badge-notification\"");
        m_openTag("span", "class=\"chart_opts\" chartid="+ToString(m_getTotalLineCharts()));
        m_putTextSource("2Pie");
        m_closeTag();
        m_closeTag();
    }
}


string* hk_reportHtml::m_prepareCharts(hk_dbconnection* it,string* params) {
    
    PGconn* conn = it->m_GetConnection();
    vector<hk_reportQueries> charts = m_getReportQueries();
    string* menuList = new string[3];
    bool isprettysql = it->m_isPrettySql();
    
    for (size_t i=0; i<charts.size(); i++) {
        
        string chartTitle = charts[i].m_getChart().m_getChartHeader();
        string query = charts[i].m_getQuery();
        string chartSize = charts[i].m_getChart().m_getChartUIKITSize();
        
        if (charts[i].m_getChartType() == LINE_CHART) {
            
            string xLabel = charts[i].m_getChart().m_getChartXLabel();
            string yLabel = charts[i].m_getChart().m_getChartYLabel();
            
            string result = m_execGetJSONArray(conn, query, params);
            m_openTag("a", "name=\"href_chart_" + ToString(m_getTotalCharts()) + "\" style=\"visibility: hidden\"");
            m_closeTag();
            m_openTag("div",	"class=\""+ chartSize +"  uk-container-center\" id=\"line_chart_"+
                      ToString(m_totalLineCharts) +"\"chart_data='"+result+"'"+
                      " xlabel=\"" + xLabel + "\""
                      " ylabel=\"" + yLabel + "\"");
            m_appendTitleOrOpts(chartTitle);
            m_openTag("svg", "class=\"chart_svg\" id=\"svg_line_"+ ToString(m_totalLineCharts)+"\"");
            m_closeTag();
            m_closeTag();
            
            if (charts[i].m_getChartLevel() == CLUSTER_LEVEL)
                menuList[CLUSTER_LEVEL]+="<li><a href=\"#href_chart_"+ToString(m_getTotalCharts())+"\"> "+ chartTitle +"</a></li>";
            else if(charts[i].m_getChartLevel() == DATABASE_LEVEL)
                menuList[DATABASE_LEVEL]+="<li><a href=\"#href_chart_"+ToString(m_getTotalCharts())+"\"> "+ chartTitle +"</a></li>";
            else if(charts[i].m_getChartLevel() == OS_LEVEL)
                menuList[OS_LEVEL]+="<li><a href=\"#href_chart_"+ToString(m_getTotalCharts())+"\"> "+ chartTitle +"</a></li>";
            ++m_totalLineCharts;
        }
        
        else if(charts[i].m_getChartType() == TABLE_CHART) {
            
            //string result = m_execGetHtmlTable(conn, query, params);
            m_openTag("a", "name=\"href_chart_" + ToString(m_getTotalCharts()) + "\" style=\"visibility: hidden\"");
            m_closeTag();
           // if (charts[i].m_getIsFiltred())
           //     m_openTag("div", "class=\"uk-width-large-3-4 uk-container-center uk-scrollable-text\" id=\"table_chart_"+ToString(m_totalTableCharts) +"\" style=\"visibility: hidden\"");
           // else
                m_openTag("div", "class=\""+ chartSize +" uk-container-center uk-scrollable-text\" id=\"table_chart_"+ToString(m_totalTableCharts) +"\"");

            m_openTag("table", "class=\"uk-table uk-table-hover uk-table-striped uk-table-condensed\" id=\"sort_table_"+ToString(m_totalTableCharts) +"\"");
            m_appendTitleOrOpts(chartTitle, false);
            //m_putTextSource(result);
            m_execGetHtmlTable(conn, query, params, false, isprettysql);
            m_closeTag();
            m_closeTag();
            if (charts[i].m_getChartLevel() == CLUSTER_LEVEL)
                menuList[CLUSTER_LEVEL]+="<li><a href=\"#href_chart_"+ToString(m_getTotalCharts())+"\"> "+ chartTitle +"</a></li>";
            else if(charts[i].m_getChartLevel() == DATABASE_LEVEL)
                menuList[DATABASE_LEVEL]+="<li><a href=\"#href_chart_"+ToString(m_getTotalCharts())+"\"> "+ chartTitle +"</a></li>";
            ++m_totalTableCharts;
        }
    }
    return menuList;
}

void hk_reportHtml::m_prepareMenu(string menuList) {
    
    m_openTag("ul", "id=\"menu\"");
    m_openTag("div", "id=\"hide_menu\"");
    m_putTextSource("Hide");
    m_closeTag();
    m_putTextSource(menuList);
    m_closeTag(); //ul
}

string hk_reportQueries::m_getQuery() {
    
    return m_query;
}

size_t hk_reportHtml::m_getTotalLineCharts() {
    
    return m_totalLineCharts;
}

size_t hk_reportHtml::m_getTotalTableCharts() {
    
    return m_totalTableCharts;
}

size_t hk_reportHtml::m_getTotalCharts() {
    
    return m_totalLineCharts + m_totalTableCharts;
}

void hk_reportHtml::m_prepareUiMenu(string* menuList) {
    
    /*
     *  Preparing the UiKit Navigation Bar
     */
    m_openTag("div", "class=\"uk-margin\"");
    m_openTag("nav", "class=\"uk-navbar\"");
    m_openTag("a", "class=\"uk-navbar-brand\" href=\"#\"");
    m_putTextSource("pgSnapShot");
    m_closeTag(); //"a"
    m_openTag("ul", "class=\"uk-navbar-nav\"");
    m_openTag("li", "class=\"uk-parent\" data-uk-dropdown=\"\"");
    m_openTag("a", "href=\"\"");
    m_putTextSource("Charts");
    m_closeTag(); //"a"
    m_openTag("div", "class=\"uk-dropdown uk-dropdown-navbar\"");
    m_openTag("ul", "class=\"uk-nav uk-nav-parent-icon\" data-uk-nav");
    
    
    /*
     * OS Level Charts
     */
    
    m_openTag("li", "class=\"uk-parent\"");
    m_openTag("a href=\"#\"");
    m_putTextSource("Os");
    m_closeTag(); //"a"
    m_openTag("ul", "class=\"uk-nav-sub\"");
    m_putTextSource(menuList[OS_LEVEL]);
    m_closeTag(); //"ul"
    m_closeTag(); //"li"
    
    /*
     * Cluster Level Charts
     */
    m_openTag("li", "class=\"uk-parent\"");
    m_openTag("a href=\"#\"");
    m_putTextSource("Cluster");
    m_closeTag(); //"a"
    m_openTag("ul", "class=\"uk-nav-sub\"");
    m_putTextSource(menuList[CLUSTER_LEVEL]);
    m_closeTag(); //"ul"
    m_closeTag(); //"li"
    
    /*
     * Database Level Charts
     */
    m_openTag("li", "class=\"uk-parent\"");
    m_openTag("a href=\"#\"");
    m_putTextSource("Database");
    m_closeTag(); //"a"
    m_openTag("ul", "class=\"uk-nav-sub\"");
    m_putTextSource(menuList[DATABASE_LEVEL]);
    m_closeTag(); //"ul"
    m_closeTag(); //"li"
    
    
    m_closeTag(); //"ul"
    m_closeTag(); //"div"
    m_closeTag(); //"li"
    m_closeTag(); //"ul"
    m_closeTag(); //"nav"
    m_closeTag(); //"div"
    
    delete[] menuList;
    menuList = NULL;
}

/*
 *  hk_PushCntToFile Functor Implementation.
 */

void hk_pushCntToFile::operator()(ofstream& file, stringstream &cnt, mutex &lck) {
    
    /*
     * If the collected HTML content, is greater than 5Kb characters,
     * flush the content to file, and free the content resource
     */
    long bytes, total_bytes=0;
    string loading="/-\\|";
    short signed int counter = 4;
    while (1) {
        
        usleep(100000);
        if (lck.try_lock()) {
            
            cnt.seekg(0,ios::end);
            bytes = cnt.tellp();
            if ( bytes >>10 > 5) {
                //lock_guard<mutex> mut(lck);
                // Flusing contents to report file.
                //
                file << cnt.str();
                // Clearing the html content, which was collected as of now.
                //
                cnt.str(string());
                cnt.clear();
                total_bytes +=bytes;
            }
            lck.unlock();
        }
        cout << "Please wait... Report is being prepared " << loading[counter++%4] << " " << (total_bytes>>10) << "(KB)\r";
        cout.flush();
    }
}

hk_dbinfo::~hk_dbinfo() {
    ;
}

hk_reportHtml::~hk_reportHtml() {
    m_tags.clear();
}