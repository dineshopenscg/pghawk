/*
 * pgHawk.cpp
 *
 *  Created on: May 24, 2014
 *      Author: dinesh
 */


#include "../include/hk_dbconnection.h"
#include "../include/hk_reportHtml.h"
#include <iostream>
#include <ctime>
#include <unistd.h>

using namespace std;

int main(int argc, char** argv)
{
    mapParser *parser = new mapParser[1];
    vector<hk_dbconnection> elephant = parser->m_ParseMapFile("hawk_map.conf");
    hk_dbconnection* hawkDBPref = &(elephant[0]);
    hk_reportHtml report(hawkDBPref->m_GetRepLocation());
    string *params = new string[2];
    
    auto err_msg=hawkDBPref->TryToGetConnection();
    if (!err_msg.empty()) {
        cout<<err_msg;
        exit(1);
    }

    hawkDBPref->m_AdjustFromToDate();
    
    params[0]=hawkDBPref->m_GetFromDate();
    params[1]=hawkDBPref->m_GetToDate();
    
    // Preparing Report
    //
    
    report.m_writeHtmlHeader();
    report.m_embeddRequiredScripts();
    
    report.m_prepareUiMenu(report.m_prepareCharts(hawkDBPref, params));
    vector<size_t> noOfCharts;
    
    noOfCharts.push_back(report.m_getTotalLineCharts());
    noOfCharts.push_back(report.m_getTotalTableCharts());
    
    // Appending the charts.js, with the given parameter
    //
    report.m_embeddScriptFileWithParams(string("web/charts.js"), noOfCharts);
    
    report.m_closeTag();
    report.m_endHtmlHeader();
    report.m_fulshRptCntToFile();
    
    /* Remove stats after report generation */
    if (hawkDBPref->m_GetTruncAfter()) {
        hawkDBPref->m_execCommand(hawkDBPref->m_GetConnection(), report.m_getTruncQuery());
    }
    cout.flush();
    cout << "\nReport generation completed.\n";
    delete[] parser;
    parser = NULL;
    
    delete[] params;
    params = NULL;
    hawkDBPref->CloseConnection();
    return 0;
}
