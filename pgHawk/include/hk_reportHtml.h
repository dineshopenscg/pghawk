/*
 * hk_reportHtml.h
 *
 *  Created on: Jul 20, 2014
 *      Author: dinesh
 * Description:
 *
 * 				Hawk report generation based on NVD3 javascript library.
 */

#ifndef HK_REPORTHTML_H_
#define HK_REPORTHTML_H_
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <unistd.h>
#include <thread>
#include "hk_dbconnection.h"
#include "utils/utilities.h"

using namespace std;

enum HK_CHART_TYPES { LINE_CHART, PIE_CHART, BAR_CHART, TABLE_CHART, UNKOWN_CHART };
static string HK_CHART_HEADER_2B_COLOR[] = {
									"#393b79", "#5254a3", "#6b6ecf", "#9c9ede", "#637939",
									"#8ca252", "#b5cf6b", "#cedb9c", "#8c6d31", "#bd9e39",
									"#e7ba52", "#e7cb94", "#843c39", "#ad494a", "#d6616b",
									"#e7969c", "#7b4173", "#a55194", "#ce6dbd", "#de9ed6"
								 	 	 	 };
enum HK_CHART_LEVEL { OS_LEVEL, CLUSTER_LEVEL, DATABASE_LEVEL, UNKNOWN_LEVEL };

class hk_dbinfo;

class hk_tablechart_pref {

public:
    hk_tablechart_pref(bool isEnableSort, bool isEnablePrettyPrint, bool isEnableFiltered);
    hk_tablechart_pref(){};
    virtual ~hk_tablechart_pref();
    bool    m_getIsEnableSort();
    bool    m_getIsEnablePrettyPrint();
    bool    m_getIsEnableFiltered();

private:
    bool    m_isEnableSort;
    bool    m_isEnablePrettyPrint;
    bool    m_isEnableFiltered;
};


class hk_linechart_pref {

public:
    hk_linechart_pref(string chartXLabel, string chartYLabel, bool isFocusOn);
    hk_linechart_pref(){};
    virtual ~hk_linechart_pref();
    string  m_getChartXLabel();
    string  m_getChartYLabel();
    bool    m_getIsFocusOn();
    
private:
    string  m_chartXLabel;
    string  m_chartYLabel;
    // This member is for a line chart,
    // and if it is on, we have to display the line chart with focus(Zoom)
    //
    bool    m_isFocusOn;
};

class hk_chart_pref : public hk_linechart_pref, public hk_tablechart_pref {

public:
    hk_chart_pref(){};
    hk_chart_pref(string chartHeader, string chartUIKITSize,
                  bool isEnableSort, bool isEnablePrettyPrint, bool isEnableFiltered);
    hk_chart_pref(string chartHeader, string chartUIKITSize,
                  string chartXLabel, string chartYLabel, bool isFocusOn);
    string m_getChartHeader();
    string m_getChartUIKITSize();
    
private:
    string  m_chartHeader;
    string  m_chartUIKITSize;
};

class hk_chart {

public:
	hk_chart();
	hk_chart(HK_CHART_LEVEL level, HK_CHART_TYPES ctype, hk_chart_pref chartPref);

	HK_CHART_TYPES m_getChartType();
	HK_CHART_LEVEL m_getChartLevel();
    hk_chart_pref  m_getChart();
protected:
    hk_chart_pref m_chart;
	HK_CHART_TYPES m_chartType;
	HK_CHART_LEVEL m_chartLevel;
};


class hk_reportQueries: public hk_chart {

public:
	hk_reportQueries(HK_CHART_LEVEL level, string query, bool isDbLevel, HK_CHART_TYPES ctype, hk_chart_pref chartPref);
	string m_getQuery();
private:
	string m_query;
	bool m_isDbLevel;
};

class hk_dbinfo {

public:
	hk_dbinfo();
	string m_getTruncQuery();
	string m_getDbNamesQuery();
	vector<hk_reportQueries> m_getReportQueries();
	~hk_dbinfo();
private:
	vector<hk_reportQueries> m_reportQueries;
	string m_truncQuery;
	string m_dbNamesQuery;
};

/*
 * Functor for an internal thread,
 * to write the report contents silently to file
 */

class hk_pushCntToFile {
public:
    void operator()(ofstream& file, stringstream& cnt, mutex& lck);
};

class hk_reportHtml: public hk_dbinfo {

public:
	hk_reportHtml(string rfile);
	void m_writeHtmlHeader();
	void m_putComment(string msg);
	void m_openTag(const string& tag, string attribs=" ");
	void m_putTextSource(const string& cnt);
	void m_closeTag();
	void m_endHtmlHeader();
	void m_embeddCssFiles(string* files);
	void m_embeddScriptFiles(string* files);
	void m_embeddScriptFileWithParams(string file, vector<size_t> params);
	void m_clearReportContent();
	bool m_fulshRptCntToFile();
	void m_openCData();
	void m_closeCData();
	void m_embeddRequiredScripts();
	string* m_prepareCharts(hk_dbconnection* it, string* params);
	void m_prepareMenu(string menu);
	size_t m_getTotalLineCharts();
	size_t m_getTotalTableCharts();
	size_t m_getTotalCharts();
	void m_appendTitleOrOpts(string title, bool isLine);
	void m_prepareUiMenu(string* menuList);
    void m_pushCntToStream(const string str);
    string m_execGetBarChartVal(PGconn* conn, string instruct);
    // This JSONArray is for generating the line chart's required data,
    // in JSON format.
    //
    string m_execGetJSONArray(PGconn* conn, string instruct, string* params);
    void m_execGetHtmlTable(PGconn* conn, string instruct, string* params, bool isCubeTable=false, bool isPrettySql=false);
    ~hk_reportHtml();
private:
	ofstream m_reportFile;
	stringstream m_reportContent;
	bool m_isReportFileOpened;
	size_t m_totalLineCharts;
	size_t m_totalTableCharts;
    vector<string> m_tags;
    mutex m_lock;
};

#endif /* HK_REPORTHTML_H_ */
