/*
 * utilities.h
 *
 *   Created on: May 27, 2014
 *       Author: dinesh
 *  Description:
 *				 Hawks, all utility function blue prints.
 */

#ifndef UTILITIES_H_
#define UTILITIES_H_

#include <iostream>
#include <sstream>
#include <fstream>
#include <ctime>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <regex>
#include "../hk_dbconnection.h"

#define CONF_MAX_ELEMENT_LENGTH 512

using namespace std;

static regex rgxOprts("(\\s+\\w+)?([\\+-\\/\\%\\* <>=]+)(\\w+)");

static regex rgxRsrvdKeyWords("\\b(ALL|ANALYSE|ANALYZE|AND|ANY|ARRAY|AS|ASC|ASYMMETRIC|BOTH|CASE|CAST|CHECK|COLLATE|COLUMN|CONSTRAINT|CREATE|CURRENT_CATALOG|CURRENT_DATE|CURRENT_ROLE|CURRENT_TIME|CURRENT_TIMESTAMP|CURRENT_USER|DEFAULT|DEFERRABLE|DESC|DISTINCT|DO|ELSE|END|EXCEPT|FALSE|FETCH|FOR|FOREIGN|FROM|GRANT|GROUP|HAVING|IN|INITIALLY|INTERSECT|INTO|LATERAL|LEADING|LIMIT|LOCALTIME|LOCALTIMESTAMP|NOT|NULL|OFFSET|ON|ONLY|OR|ORDER|PLACING|PRIMARY|REFERENCES|RETURNING|SELECT|SESSION_USER|SOME|SYMMETRIC|TABLE|THEN|TO|TRAILING|TRUE|UNION|UNIQUE|USER|USING|VARIADIC|WHEN|WHERE|WINDOW|WITH|AUTHORIZATION|BINARY|COLLATION|CONCURRENTLY|CROSS|CURRENT_SCHEMA|FREEZE|FULL|ILIKE|INNER|IS|ISNULL|JOIN|LEFT|LIKE|NATURAL|NOTNULL|OUTER|OVER|OVERLAPS|RIGHT|SIMILAR|VERBOSE)(?=[^\\w])", regex_constants::icase);

static regex rgxUnrsrvdKeyWords("\\b(ABORT|ABSOLUTE|ACCESS|ACTION|ADD|ADMIN|AFTER|AGGREGATE|ALSO|ALTER|ALWAYS|ASSERTION|ASSIGNMENT|AT|ATTRIBUTE|BACKWARD|BEFORE|BEGIN|BY|CACHE|CALLED|CASCADE|CASCADED|CATALOG|CHAIN|CHARACTERISTICS|CHECKPOINT|CLOSE|CLUSTER|COMMENT|COMMENTS|COMMIT|COMMITTED|CONFIGURATION|CONNECTION|CONSTRAINTS|CONTENT|CONTINUE|CONVERSION|COPY|COST|CSV|CURRENT|CURSOR|CYCLE|DATA|DATABASE|DAY|DEALLOCATE|DECLARE|DEFAULTS|DEFERRED|DEFINER|DELETE|DELIMITER|DELIMITERS|DICTIONARY|DISABLE|DISCARD|DOCUMENT|DOMAIN|DOUBLE|DROP|EACH|ENABLE|ENCODING|ENCRYPTED|ENUM|ESCAPE|EVENT|EXCLUDE|EXCLUDING|EXCLUSIVE|EXECUTE|EXPLAIN|EXTENSION|EXTERNAL|FAMILY|FIRST|FOLLOWING|FORCE|FORWARD|FUNCTION|FUNCTIONS|GLOBAL|GRANTED|HANDLER|HEADER|HOLD|HOUR|IDENTITY|IF|IMMEDIATE|IMMUTABLE|IMPLICIT|INCLUDING|INCREMENT|INDEX|INDEXES|INHERIT|INHERITS|INLINE|INPUT|INSENSITIVE|INSERT|INSTEAD|INVOKER|ISOLATION|KEY|LABEL|LANGUAGE|LARGE|LAST|LC_COLLATE|LC_CTYPE|LEAKPROOF|LEVEL|LISTEN|LOAD|LOCAL|LOCATION|LOCK|MAPPING|MATCH|MATERIALIZED|MAXVALUE|MINUTE|MINVALUE|MODE|MONTH|MOVE|NAME|NAMES|NEXT|NO|NOTHING|NOTIFY|NOWAIT|NULLS|OBJECT|OF|OFF|OIDS|OPERATOR|OPTION|OPTIONS|OWNED|OWNER|PARSER|PARTIAL|PARTITION|PASSING|PASSWORD|PLANS|PRECEDING|PREPARE|PREPARED|PRESERVE|PRIOR|PRIVILEGES|PROCEDURAL|PROCEDURE|PROGRAM|QUOTE|RANGE|READ|REASSIGN|RECHECK|RECURSIVE|REF|REFRESH|REINDEX|RELATIVE|RELEASE|RENAME|REPEATABLE|REPLACE|REPLICA|RESET|RESTART|RESTRICT|RETURNS|REVOKE|ROLE|ROLLBACK|ROWS|RULE|SAVEPOINT|SCHEMA|SCROLL|SEARCH|SECOND|SECURITY|SEQUENCE|SEQUENCES|SERIALIZABLE|SERVER|SESSION|SET|SHARE|SHOW|SIMPLE|SNAPSHOT|STABLE|STANDALONE|START|STATEMENT|STATISTICS|STDIN|STDOUT|STORAGE|STRICT|STRIP|SYSID|SYSTEM|TABLES|TABLESPACE|TEMP|TEMPLATE|TEMPORARY|TEXT|TRANSACTION|TRIGGER|TRUNCATE|TRUSTED|TYPE|TYPES|UNBOUNDED|UNCOMMITTED|UNENCRYPTED|UNKNOWN|UNLISTEN|UNLOGGED|UNTIL|UPDATE|VACUUM|VALID|VALIDATE|VALIDATOR|VALUE|VARYING|VERSION|VIEW|VOLATILE|WHITESPACE|WITHOUT|WORK|WRAPPER|WRITE|XML|YEAR|YES|ZONE|BETWEEN|BIGINT|BIT|BOOLEAN|CHAR|CHARACTER|COALESCE|DEC|DECIMAL|EXISTS|EXTRACT|FLOAT|GREATEST|INOUT|INT|INTEGER|INTERVAL|LEAST|NATIONAL|NCHAR|NONE|NULLIF|NUMERIC|OUT|OVERLAY|POSITION|PRECISION|REAL|ROW|SETOF|SMALLINT|SUBSTRING|TIME|TIMESTAMP|TREAT|TRIM|VALUES|VARCHAR|XMLATTRIBUTES|XMLCONCAT|XMLELEMENT|XMLEXISTS|XMLFOREST|XMLPARSE|XMLPI|XMLROOT|XMLSERIALIZE)(?=[^\\w])", regex_constants::icase);
enum Log_Level{ERROR, WARNING, NOTICE};

class logger {

public:
	logger(const char* f);
	bool m_OpenLogfile(const char* f);
	void m_LogMessage(Log_Level l, const char* msg, bool appendNewLine=false);
	bool m_CloseLogfile();
	~logger();
private:
	ofstream m_Logfile;
	pthread_mutex_t m_mutex_lock;
};

class mapParser {

public:
	//char* m_ParseAndGetEleId(const char* line);
	string m_ParseAndGet(const char* line, const char* getMe);
	vector<hk_dbconnection> m_ParseMapFile(const char* mapFile);

/*private:
	FILE* m_mapFile;
	hk_dbconnection* m_elephantSet;
 */
};

// Get the current timestamp value as string
//
string GetCurrentTimeStamp();

// Trim the white spaces from both sides of the string
//
char* TrimWhiteSpaces(const char* line);

// Conver num to string
//
string ToString(size_t num);

// Convert pgArray to jsArray
//
string ToJsArray(string value);

// Converting to Quoted String
//
string ToQuoteString(string value);

// Checking the string is a number or not
//
bool IsNumber(string& str);

// SQL Pretty Print
//
string sqlPrettyPrint(string& query);

#endif /* UTILITIES_H_ */
