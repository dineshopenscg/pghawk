/*
 * hk_dbconnection.h
 *
 *   Created on: Jun 17, 2014
 *       Author: dinesh
 *  Description:
 *				 It maintains the hawk's Arena (Monitoring) connection,
 *				 Statswarehouse (Centralized Monitered stats storage) connections.
 */

#ifndef HK_DBCONNECTION_H_
#define HK_DBCONNECTION_H_

#include "libpq-fe.h"
#include <string>
#include <strstream>
#include <vector>
#include <ctime>

#define DEFAULT_ELEPHANT_VERSION 90304

using namespace std;

// All PostgreSQL data retrival operations should be here.
//
class hk_dboperations {

public:
	string m_execGetScalar(PGconn* conn, string instruct);
	bool m_execInsrtUpdte(PGconn* conn, string instruct, string relname);
	bool m_execCommand(PGconn* conn, string instruct);
	vector<string> m_execGet1DArray(PGconn* conn, string instruct);
};

class hk_dbconnection: public hk_dboperations {

public:
	hk_dbconnection();
	//hk_dbconnection(string host, string dbname, string user, string password, size_t port, size_t ver, size_t eleid);
	const char* GetConnectionString();
	bool IsConnectionLive();
	string TryToGetConnection();
	bool m_GetTruncAfter();
    bool m_isPrettySql();
	void CloseConnection();
	string GetHost();
	string GetDbname();
	string GetUser();
	string GetPassword();
	size_t GetPort();
	size_t GetVersion();
	size_t m_GetWatchTime();
	size_t m_GetNapTime();
	string m_GetElephantId();
	string m_GetFromDate();
	string m_GetToDate();
    string m_GetRepeatRep();
    string m_GetFileName();
    string m_GetRepLocation();
    
	void m_SetElephantId(string eleid);
	void SetHost(string host);
	void SetDbname(string dbname);
	void SetUser(string user);
	void SetPort(size_t port);
	void SetPassword(string password);
	void SetVersion(size_t ver);
	void m_SetNapTime(size_t nt);
	void m_SetWatchTime(size_t wt);
	void m_SetTruncAfter(bool drpPrev);
    void m_SetPrettySql(bool ispretty);
	void m_SetToDate(string todate);
	void m_SetFromDate(string fromdate);
    void m_SetRepeatRep(string repeatrep);
    void m_AdjustFromToDate();
    void m_SetRepLocation(string repLoc);
    void m_SetFileName(string filename);
	pg_conn* m_GetConnection();
	~hk_dbconnection();
private:

	string m_host, m_dbname, m_user, m_password, m_eleid, m_fromdate, m_todate, m_repeatRep, m_filename, m_replocation;
	size_t m_port, m_version, m_napTime, m_watchTime;
	pg_conn* m_conn;
	bool m_connection_live, m_truncafter, m_prettySql;
};


#endif /* HK_DBCONNECTION_H_ */
