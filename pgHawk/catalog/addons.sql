
/*
 *	 			pgHawk
 * 
 * cols_xcept_substitue() will be useful to get the rest of the columns,
 * by ignoring the given list of columns.
 * 
 * And also, it will substitue the column with another column name.
 *
*/

CREATE OR REPLACE FUNCTION COLS_XCEPT_SUBSTITUE(tabname TEXT,
						colnames TEXT[],
						fromcols TEXT[],
						tocols TEXT[])
RETURNS TEXT AS $$
DECLARE
columnCheck INT:=0;
noOfCols INT:=0;
restCols TEXT:=' ';
rec RECORD;
i INT;
BEGIN
		
	SELECT COUNT(*) INTO noOfCols
	FROM information_schema.columns
	WHERE table_name=LOWER(tabname);
	
	IF (noOfCols>0) THEN
		-- Table found, with the more than one column.
		-- Remove the given columns from the list, and prepare a text
		-- of rest of the column names.
		FOR rec IN (
				SELECT column_name
				FROM information_schema.columns
				WHERE table_name=LOWER(tabname)
			   ) LOOP
			FOR i IN 1..noOfCols LOOP
				IF (LOWER(colnames[i])::TEXT=LOWER(rec.column_name)) THEN
					columnCheck:=1;
				END IF;
			END LOOP;
			IF(columnCheck=0) THEN
				restCols:=restCols||rec.column_name||',';
			END IF;
			columnCheck:=0;
		END LOOP;
		
		-- Replacing the columns with the new columns
		-- This will be benificial in pg_stat_activity catalog,
		-- We have procpid in <=PG 9.1 versions, and pid in greater versions.
		--
		FOR i IN 1..array_length(fromcols, 1) LOOP
			restCols:=REPLACE(restCols, fromcols[i], tocols[i]);
		END LOOP;
		restCols:=TRIM(restCols, ',');
		RETURN restCols;
		
	ELSE
		-- Invalid table, hence return NULL.
		RETURN NULL;
	END IF;
END;
$$ LANGUAGE PLPGSQL;

