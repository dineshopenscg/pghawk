-- SnapshottingDbs.sql
-- V1.1
--
-- SRM: Scott Mead, OpenSCG, inc
-- scottm@openscg.com
-- This script is released under the GPL v3 + Attribution
-- 
-- This script will setup a snapshot infrastructure inside of a 
-- postgres database.  It will monitor table usage and growth 
-- statistics and let you report over them
-- 
-- Usage:
--  1. Run this script in its entirty in a database
--     - You must have privilege to create schemas, tables, sequences
--        types and functions as well as read the pg_stat_user_table view
--
-- 2. Run a cronjob to periodically create a snapshot
--  i.e. 
--  */5 * * * * psql -d db -U user -c "select snapshots.save_snap();"
--   
-- 3. To view available snapshots for reporting, run:
--  select * from snapshots.list_snaps();
--
-- 4. To generate a report, run
--  select * from report_tables(from snap_id , to snap_id);
--  select * from report_indexes(from snap_id , to snap_id);
-- 
--  i.e.
--  select * from report_tables(1,100) will generate a report
--  of what happened between snapshot 1 and 100
--
-- To just see a report of the MAX snapshot, you can run:
-- 
--  select * from report_indexes(1,0);
--  Using a '0' will automatically choose the highest snapshot
--  Using a '0' in the 'from' snap field will choose the MAX snap -1
--
-- Changelog
-- Author|   Date      |  Ver. |  Comment
-- ===================================================
-- SRM   | 2014-02-05  |  1.0  |  First version
-- SRM   | 2014-02-06  |  1.1  |  * Renamed table for table stats
--                             |  * Maintaining separate snap list 
--                             |  * Added saving and reporting of index stats
--                             |  * Added saving of stat_activity ( no reporting yet )

create schema snapshots;

--Create table
create table snapshots.snap_user_tables as 
   select 1 snap_id, now() dttm, *, pg_relation_size(relid::regclass) relsize,
               pg_total_relation_size(relid::regclass) totalrelsize 
     from pg_stat_user_tables; 

create table snapshots.snap_stat_activity as 
   select 1 snap_id, now() dttm, *
     from pg_stat_activity; 

create table snapshots.snap_indexes as 
   select 1 snap_id, now() dttm, *,
        pg_relation_size(relid::regclass) relsize, 
        pg_total_relation_size(relid::regclass) totalrelsize
     from pg_stat_user_indexes; 

create table snapshots.snap_database as
   select 1 snap_id, now() dttm, *, pg_database_size(datname) as dbsize
     from pg_stat_database;

create table snapshots.snap_xlog_location as
   select 1 snap_id,* FROM pg_xlogfile_name_offset(pg_current_xlog_location());

create table snapshots.snap as
    SELECT 1 snap_id, now() dttm;

create index idx_snap_snap_id on snapshots.snap(snap_id);
create index idx_snap_user_tables_snap_id on snapshots.snap_user_tables(snap_id);
create index idx_snap_stat_activity_snap_id on snapshots.snap_stat_activity(snap_id);
create index idx_snap_indexes_snap_id on snapshots.snap_indexes(snap_id);
create index idx_snap_database_snap_id on snapshots.snap_database(snap_id);

create sequence snapshots.snap_seq start 2;

--Insert into snap
create or replace function snapshots.save_snap () RETURNS VOID as $_$
DECLARE
     snap_id INT;
BEGIN
    snap_id := nextval('snapshots.snap_seq');

    insert into snapshots.snap select snap_id, now() ;

    insert into snapshots.snap_user_tables select snap_id, now(), *, pg_relation_size(relid::regclass) relsize,
               pg_total_relation_size(relid::regclass) totalrelsize from pg_stat_user_tables;

    insert into snapshots.snap_stat_activity SELECT snap_id, now(), * from pg_stat_activity;

    insert into snapshots.snap_indexes SELECT snap_id, now(), * , 
                pg_relation_size(relid::regclass) relsize, pg_total_relation_size(relid::regclass) totalrelsize 
            from pg_stat_user_indexes;
    
    insert into snapshots.snap_database SELECT snap_id, now(), *, pg_database_size(datname) FROM pg_stat_database;

    insert into snapshots.snap_xlog_location SELECT snap_id, * FROM pg_xlogfile_name_offset(pg_current_xlog_location());

END; $_$ language 'plpgsql'; 


select snapshots.save_snap(); 

create type snapshots.snap_list AS ( snaps_id int, dttm timestamp with time zone ) ; 

create or replace function snapshots.list_snaps() RETURNS SETOF snapshots.snap_list AS $_$
     select distinct snap_id, dttm from snapshots.snap order by snap_id;
 $_$ language 'sql';


create type snapshots.report_tables_record AS ( time_window interval, relname name, ins bigint, upd bigint, del bigint,
                                                 index_scan bigint, seqscan bigint, relsize_growth_bytes bigint, relsize_growth text, 
                                                 total_relsize_growth_bytes bigint, total_relsize_growth text );

create or replace function snapshots.report_tables ( snap_from INT, snap_to INT ) RETURNS SETOF snapshots.report_tables_record AS $_$
DECLARE
     start_id INT;
     end_id  INT;
     start_date TIMESTAMP WITH TIME ZONE;
     end_date TIMESTAMP WITH TIME ZONE; 
     query TEXT;
     
BEGIN
     
     IF snap_to = 0
     THEN
          select into end_id max(snap_id) from snapshots.snap_user_tables ;
     ELSE
          end_id := snap_to;
     END IF;

     select into end_date dttm from snapshots.snap where snap_id = end_id limit 1; 

     IF snap_from = 0 
     THEN
          start_id = end_id - 1;
     ELSE
          start_id = snap_from;
     END IF;

    select into start_date dttm from snapshots.snap where snap_id = start_id limit 1; 

     RAISE NOTICE 'Report   From  Snapshot # % Taken at %' , start_id, start_date ;
     RAISE NOTICE 'Report   To       Snapshot # % Taken at %' , end_id, end_date ;

     query := 'select b.dttm - a.dttm ,  b.relname, b.n_tup_ins - a.n_tup_ins ins, b.n_tup_upd - a.n_tup_upd upd, b.n_tup_del - a.n_tup_del del, '
             || 'b.idx_scan - a.idx_scan index_scan, b.seq_scan - a.seq_scan seqscan, b.relsize - a.relsize relsize_growth_bytes, '
             || 'pg_size_pretty( b.relsize - a.relsize) relsize_growth, b.totalrelsize - a.totalrelsize total_relsize_growth_bytes, '
             || 'pg_size_pretty(b.totalrelsize - a.totalrelsize) total_relsize_growth '
             || 'from snapshots.snap_user_tables a , snapshots.snap_user_tables b '
             || 'where a.snap_id=$1 '
             || 'and b.snap_id=$2 '
             || 'and a.relid=b.relid '
             || 'order by ( (b.n_tup_ins - a.n_tup_ins ) + (b.n_tup_upd - a.n_tup_upd ) + (b.n_tup_del - a.n_tup_del)) desc ';

     RETURN QUERY EXECUTE query USING start_id, end_id;

END; $_$ language 'plpgsql';


create type snapshots.report_indexes_record AS ( time_window interval, relname name, indexrelname name, idx_scan bigint, 
                                                 idx_tup_read bigint, idx_tup_fetch bigint, relsize_growth_bytes bigint,relsize_growth text,
                                                 total_relsize_growth_bytes bigint, total_relsize_growth text );

create or replace function snapshots.report_indexes ( snap_from INT, snap_to INT ) RETURNS SETOF snapshots.report_indexes_record AS $_$
DECLARE
     start_id INT;
     end_id  INT;
     start_date TIMESTAMP WITH TIME ZONE;
     end_date TIMESTAMP WITH TIME ZONE; 
     query TEXT;
     
BEGIN
     
     IF snap_to = 0
     THEN
          select into end_id max(snap_id) from snapshots.snap ;
     ELSE
          end_id := snap_to;
     END IF;

     select into end_date dttm from snapshots.snap where snap_id = end_id limit 1; 

     IF snap_from = 0 
     THEN
          start_id = end_id - 1;
     ELSE
          start_id = snap_from;
     END IF;

    select into start_date dttm from snapshots.snap where snap_id = start_id limit 1; 

     RAISE NOTICE 'Report   From  Snapshot # % Taken at %' , start_id, start_date ;
     RAISE NOTICE 'Report   To       Snapshot # % Taken at %' , end_id, end_date ;

     query := 'select b.dttm - a.dttm ,  b.relname, b.indexrelname, b.idx_scan - a.idx_scan idx_scan, '
             || 'b.idx_tup_read - a.idx_tup_read idx_tup_read, b.idx_tup_fetch - a.idx_tup_fetch idx_tup_fetch, b.relsize - a.relsize relsize_growth_bytes, '
             || 'pg_size_pretty( b.relsize - a.relsize) relsize_growth, b.totalrelsize - a.totalrelsize total_relsize_growth_bytes, '
             || 'pg_size_pretty(b.totalrelsize - a.totalrelsize) total_relsize_growth '
             || 'from snapshots.snap_indexes a , snapshots.snap_indexes b '
             || 'where a.snap_id=$1 '
             || 'and b.snap_id=$2 '
             || 'and a.relid=b.relid '
             || 'order by (b.idx_scan - a.idx_scan )  desc ';

     RETURN QUERY EXECUTE query USING start_id, end_id;

END; $_$ language 'plpgsql';

