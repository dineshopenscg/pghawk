/*
 * pgHawk's Charts.js
 *
 * This is responsible for the complete chart operations.
 *
 */


// totalLineCharts, totalBarCharts variable values, will be replacing with the proper values,
// while loading this script into the html report.
//
var totalLineCharts = $1;
var totalTableCharts = $2;
var i;

// Generating Line Charts
//
for(i=0; i<totalLineCharts; i++) {
	generateLineChart(i);
}

// Generating Sortable Table Charts
//
for(i=0; i<totalTableCharts; i++) {
	/*
	 * Tablesort is the method from the "tablesort.min.js",
	 * Which is a table sort library.
	 */
	new Tablesort(document.getElementById('sort_table_'+i));
}

function toTimeFromEpoch(d, i) {
	var now;
	now = new Date(d*1000);
	return d3.time.format('%H:%M:%S')(now);
}

function generateLineChart(i) {
	var chart;
	var xLabel = document.getElementById("line_chart_"+i).getAttribute("xlabel");
	var yLabel = document.getElementById("line_chart_"+i).getAttribute("ylabel");

	nv.addGraph(function() {
	chart = nv.models.lineChart()
	.options({
	margin: {left: 100, bottom: 100},
	x: function(d,i) { return i},
	showXAxis: true,
	showYAxis: true,
	transitionDuration: 250});

	// X-Axis value options
	//
	chart.x(function(d,i) { return d.x });
	chart.xAxis
	.axisLabel(xLabel)
	.tickFormat(function(d, i){
	var now;
	now = new Date(d*1000);
	return d3.time.format('%m-%d-%y %H:%M:%S')(now);
		});

	// Y-Axis value options
	//
	chart.yAxis
	.axisLabel("Total")
	.tickFormat(function(d){
		if (d/100000>=1)
			return (d/100000).toFixed(1)+" G";
		if (d/10000>=1)
			return (d/10000).toFixed(1)+" M";
		else if (d/1000>=1)
			return (d/1000).toFixed(1)+" K";
		else
			return d.toFixed(2);
	});

	 // Removing the old chart content.
	 //

	 d3.select('#svg_line_'+i).selectAll("svg > *").remove();
	 d3.select('#svg_line_'+i) // Line chart SVG.
	.datum(line_data('line_chart_'+i)) // Line chart Div, which holds the required data.
	.call(chart);

	// Window resize option
	//
	nv.utils.windowResize(chart.update);
	return chart;
	});
}

function generateFocusLineChart(i) {
	var chart;
	var xLabel = document.getElementById("line_chart_"+i).getAttribute("xlabel");
	var yLabel = document.getElementById("line_chart_"+i).getAttribute("ylabel");

	nv.addGraph(function() {
	chart = nv.models.lineWithFocusChart().margin({top: 30, left: 80, bottom: 50});

	// X-Axis value options
	//
	chart.x(function(d,i) { return d.x });
	chart.xAxis
	.axisLabel(xLabel)
	.tickFormat(function(d, i){
	var now;
	now = new Date(d*1000);
	return d3.time.format('%m-%d-%y %H:%M:%S')(now);
		});

  chart.x2Axis
      .tickFormat(function(d, i){
	var now;
	now = new Date(d*1000);
	return d3.time.format('%m-%d-%y %H:%M:%S')(now);
		});

  chart.yAxis
	.axisLabel("Total")
	//.tickFormat(d3.format(',.2f'));
	.tickFormat(function(d){
		if (d/100000>=1)
			return (d/100000).toFixed(1)+" G";
		if (d/10000>=1)
			return (d/10000).toFixed(1)+" M";
		else if (d/1000>=1)
			return (d/1000).toFixed(1)+" K";
		else
			return d.toFixed(2);
	});
  chart.y2Axis
      .tickFormat(function(d){
		if (d/100000>=1)
			return (d/100000).toFixed(1)+" G";
		if (d/10000>=1)
			return (d/10000).toFixed(1)+" M";
		else if (d/1000>=1)
			return (d/1000).toFixed(1)+" K";
		else
			return d.toFixed(2);
	});

	 // Removing the old chart content.
	 //
	d3.select('#svg_line_'+i).selectAll("svg > *").remove();

	d3.select('#svg_line_'+i) 			// Line chart SVG.
	.datum(line_data('line_chart_'+i)) // Line chart Div, which holds the required data.
	.call(chart);

	// Window resize option
	//
	nv.utils.windowResize(chart.update);
	return chart;
	});
}

function generatePieChart(i) {
	var chart;
	var colors = new Array("#FF9966", "#FF6699", "#3399FF", "#33ADAD", "#CCCC00", "#3030AC", "#A31947");

	nv.addGraph(function() {
	chart = nv.models.pieChart()
      .x(function(d) { return d.label })
      .y(function(d) { return d.value })
      .showLabels(true)     //Display pie labels
      .labelThreshold(.05)  //Configure the minimum slice size for labels to show up
      .labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
      .donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
      .donutRatio(0.2)
      .color(colors);

	// Removing the old chart content.
	//
	d3.select('#svg_line_'+i).selectAll("svg > *").remove();

	d3.select('#svg_line_'+i)
	.datum(piedata('line_chart_'+i))
	.call(chart);

    // Window resize option
	//
	nv.utils.windowResize(chart.update);
	return chart;
	});
}

function piedata(id) {
	var json_values = [],
		labels = [],
		arr_values = [],
		data = [];

	data = JSON.parse(document.getElementById(id).getAttribute("chart_data"));

	for (var key in data) {
		for (var rec in data[key]) {
			if (key=="data") {
				var labelValsSum=0;
				for (var val in data[key][rec])
						labelValsSum+=data[key][rec][val]["y"];
				arr_values.push(labelValsSum);
			}
			else
				labels.push(data[key][rec]);
		}
	}

	for (var i=0;i<labels.length; i++)
		json_values.push({label:labels[i], value:arr_values[i]});

	return json_values;
}

function line_data(id) {

	var data = [],
	json_values = [],
	arr_values = [],
	lables = [];
	var colors = new Array("#FF9966", "#FF6699", "#3399FF", "#33ADAD", "#CCCC00", "#3030AC", "#A31947");

	data = JSON.parse(document.getElementById(id).getAttribute("chart_data"));

	for(var key in data) {
		for(var rec in data[key]) {
			if (key=="data")
				arr_values.push(data[key][rec]);
			else
				lables.push(data[key][rec]);
		}
	}

	for (var i=0; i<lables.length; i++)
		json_values.push({values: arr_values[i], key:lables[i], color: colors[i]});

	return json_values;
};


function generateBarChart(i) {

var chart;
	nv.addGraph(function() {

		var data = JSON.parse(document.getElementById("bar_chart_"+i).getAttribute("chart_data"));
		var arr = [];

		for (var val in data["data"]) {
			arr.push(data["data"][val]);
		}
		var barData = [];
		barData.push({values: arr});

		chart = nv.models.discreteBarChart()
					.x(function(d) { return d.label })
					.y(function(d) { return d.value })
					.staggerLabels(true)
					.tooltips(true)
					.showValues(true)
					.transitionDuration(250);

		d3.select('#svg_bar_'+i)
		.datum(barData)
		.call(chart);
		nv.utils.windowResize(chart.update);
		return chart;
		});
}

/*
 * Menu UI hide and un-hide
 */
 /*
var menuUi = document.getElementById("menu");
var hideMenu = document.getElementById("hide_menu");

hideMenu.addEventListener('click', function(e) {
	/*
	 * Stopping the javascript event propogation to it's parent item.
	 *
	 * Here, "hide_menu" is the inner div item in "menu" ui list.
	 * If we click on "hide_menu" javascript raises two events, one is one inner and other on outer div items.
	 * Hence, stopping the outer click event in this case, which is not required.
	 */
/*
	if (!e) var e = window.event;
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();

	menuUi.setAttribute("style","right:-150px");
});

menuUi.addEventListener('click', function(e) {
	menuUi.setAttribute("style","right:0px");
});

*/

/*
 * Menu Wheel
 */
/*
nv.addGraph(function() {

    var width = 310,
        height = 310;

	var wheelData=[];
	var totalCharts=document.getElementById("menuWheel").getAttribute("totalcharts");

	/*
	 * Pushing the wheel slices data
	 */
/*
	for (var i=0; i<totalCharts; i++) {
		wheelData.push({key:i+1, y: 1});
	}

    var chart = nv.models.pieChart()
        .x(function(d) { return d.key })
        .showLegend(false)
        .color(d3.scale.category20b().range())
        .width(width)
        .height(height)
        .donut(true)
        .donutRatio(0.65)
        .tooltips(false);

    chart.pie
        .startAngle(function(d) { return d.startAngle - Math.PI/2})
        .endAngle(function(d) { return d.endAngle - Math.PI/2 });

      d3.select("#menuWheel")
		.datum(wheelData)
		.transition().duration(1200)
		.attr('width', width)
		.attr('height', height)
		.call(chart);

    d3.selectAll(".nv-slice").on("click", function (obj) {

    /* As the chart counter starts from 0, and chart wheel shows the charts from 1 onwards,
     * we need to decrement the key value by 1 to get the exact chart location.
     */
 /*   window.location='#href_chart_'+(obj["data"]["key"]-1);
    });
    return chart;
});


document.getElementById("menu_hide_id_a").onclick=function(){menuMouseOver()};
*/

/*
function menuMouseOver() {

	var css = document.createElement("style");
	css.type = "text/css";

	if (document.getElementById("menu_hide_id_a").textContent.search("Hide")!=-1) {
		css.innerHTML = "#menuWheel {visibility: hidden}";
		document.getElementById("menu_hide_id_a").textContent="Wheel";
	}
	else {
		css.innerHTML = "#menuWheel {visibility: visible}";
		document.getElementById("menu_hide_id_a").textContent="Hide";
	}

	document.body.appendChild(css);
}
*/

window.onload = function() {

	var spans = document.getElementsByTagName('span');

	for(var i = 0; i < spans.length; i++) {
		var span = spans[i];

		if (span.textContent.search("Zoom")!=-1) {
				span.onclick = function() {
				if (this.textContent.search("Zoom")!=-1) {
					generateFocusLineChart(this.getAttribute("chartid"));
					this.textContent="Refresh";
				}
				else {
					generateLineChart(this.getAttribute("chartid"));
					this.textContent="Zoom";
				}
			}
		}
		else if (span.textContent.search("2Pie")!=-1) {
				span.onclick = function() {
				if (this.textContent.search("2Pie")!=-1){
				generatePieChart(this.getAttribute("chartid"));
				this.textContent="2Line";
			}
			else {
				generateLineChart(this.getAttribute("chartid"));
				this.textContent="2Pie";
			}
		}	
		}
	}
}
