# pgHawk #

* OpenWatch Report Generator For PostgreSQL

pgHawk generates the reports which have been collected by the openWatch. This is the reporting tool, which we can schedule to generate the reports
from the collected metrics.

How To Build In Mac
===================

Step 1:
			Go to pgHawk folder, and setup pg_config in $PATH.
			
			Ex:-
					Dineshs-MacBook-Pro:pgHawk Dinesh$ cd /Users/Dinesh/Projects/pgHawk
					Dineshs-MacBook-Pro:pgHawk Dinesh$ export PATH=$PATH:/Library/PostgreSQL/9.4/bin

Step 2:
			Run `make config`
			
			Ex:-
					Dineshs-MacBook-Pro:pgHawk Dinesh$ make config
					pg_config|grep -E '^INCLUDEDIR\s'|cut -d '=' -f2 > .pginclude
					pg_config|grep -E '^LIBDIR\s'|cut -d '=' -f2 > .pglib

Step 3:
			Run `make`
			
			Ex:-
					Dineshs-MacBook-Pro:pgHawk Dinesh$ make
					g++ -arch x86_64 -std=c++11 -stdlib=libc++ -c -Wall -I /Library/PostgreSQL/9.4/include source/*.cpp source/utils/*.cpp

Step 4:
			Run `make install`
			
			Ex:-
					Dineshs-MacBook-Pro:pgHawk Dinesh$ make install
					g++ -arch x86_64 -std=c++11 -stdlib=libc++ *.o -o pgHawk -L /Library/PostgreSQL/9.4/lib -lpq -lpthread
					
Step 5:
			Run `./pgHawk`
			
			Ex:-
					Dineshs-MacBook-Pro:pgHawk Dinesh$ ./pgHawk 
					Please wait... Report is being prepared \ 1715(KB)
					Report generation completed.
					
As pgHawk is based on it's configuration file, which we need to create a file "hawk_map.conf" in /tmp location.

hawk_map.conf Settings
======================
        
		[
                Hostip: 127.0.0.1
                User: postgres
                Port: 5432
                Dbname: postgres
                Password: postgres

                # Truncate the complete statistical data
                # from the snapshot tables
                TruncAfterRep: no

                # Enable PrettyPrint for the SQL statements in 
                # the report. If you want quick report, then 
                # make this setting as always "no"
                #
                PrettySQL: no

                # %D => Day     %d  => Day in number       %h  =>  Hour in number
                # %M => Month   %m  => Month in number     %mi =>  Minutes in number
                # %Y => Year    %y  => two digit year      %s  =>  Seconds in number
                Filename: Hawk_%Y_%M_%D.html

                # Report location 
                #
                ReportLoc: /tmp/

                # Repeat Report daily Or weekly Or monthly
                # Off to disable repeated reports.
                RepeatRepFor: off

                # Provide the date format as below format.
                # DD-MON-YYYY HH24:MI:SS
                FromDate: 23-JULY-2015 00:00:00
                ToDate: 25-JULY-2015 00:00:00
        ]
